using NUnit.Framework;
using FarPrototype.Model;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace FarPrototypeTests
{
    public class Tests
    {
        private Model model = new Model();

        [TestCase(ExpectedResult = true)]
        public bool TestGetItemsFrom()
        {
            var res = model.GetItemsFrom(@"D:\Programming\Projects\C#\FarPrototype\testFolder1");

            var list = new List<string> {
                "subFolder1",
                "subFolder2",
                "subFolder3",
                "subFolder4",
                "file1.txt",
                "file2.txt"
            };

            bool pass = true;
            for (int i = 0; i < res.Count; i++)
            {
                if (res[i] != list[i])
                {
                    pass = false;
                    break;
                }
            }

            return pass;
        }

        [TestCase(@"D:\Programming\Projects\C#\FarPrototype\testFolder1\file1.txt",
            @"D:\Programming\Projects\C#\FarPrototype\testFolder2",
            ExpectedResult = true)]
        [TestCase(@"D:\Programming\Projects\C#\FarPrototype\testFolder1\subFolder1",
            @"D:\Programming\Projects\C#\FarPrototype\testFolder2",
            ExpectedResult = true)]
        [TestCase(@"D:\Programming\Projects\C#\FarPrototype\testFolder1\subFolder2",
            @"D:\Programming\Projects\C#\FarPrototype\testFolder2",
            ExpectedResult = true)]
        public bool TestCopyTo(string itemName, string destDirName)
        {
            if (Directory.Exists(destDirName))
                Directory.Delete(destDirName, true);

            Directory.CreateDirectory(destDirName);

            model.CopySourceTo(itemName, destDirName);

            if (!Directory.Exists(itemName))
            {
                if (!Directory.GetFiles(destDirName).Contains(Path.Combine(destDirName, Path.GetFileName(itemName))))
                    return false;
                return true;
            }
            else
            {
                return checkDirectoryContainer(itemName, destDirName);
            }
        }

        public bool checkDirectoryContainer(string itemName, string destDirName)
        {
            var sourceInfo = new DirectoryInfo(itemName);
            var destInfo = new DirectoryInfo(Path.Combine(destDirName, Path.GetFileName(itemName)));

            foreach (var file in sourceInfo.GetFiles())
            {
                if (!File.Exists(Path.Combine(destInfo.FullName, file.Name)))
                    return false;
            }

            foreach (var directory in sourceInfo.GetDirectories())
            {
                if (!Directory.Exists(Path.Combine(destInfo.FullName, directory.Name)))
                    return false;

                if (!checkDirectoryContainer(directory.FullName, destInfo.FullName))
                    return false;
            }

            return true;
        }
    }
}