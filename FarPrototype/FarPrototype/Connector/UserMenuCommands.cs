﻿using FarPrototype.View.Windows;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;

namespace FarPrototype.Connector
{


    class UserMenuMoveCursorUpCommand : IUserMenuConnector
    {
        public UserMenuMoveCursorUpCommand(UserMenu menu) : base(null, menu) { }
        public override void Execute()
        {
            menu.ChangeActiveItem(ConsoleKey.UpArrow);
        }
    }

    class UserMenuMoveCursorDownCommand : IUserMenuConnector
    {
        public UserMenuMoveCursorDownCommand(UserMenu menu) : base(null, menu) { }
        public override void Execute()
        {
            menu.ChangeActiveItem(ConsoleKey.DownArrow);
        }
    }

    class UserMenuMoveCursorLeftCommand : IUserMenuConnector
    {
        public UserMenuMoveCursorLeftCommand(UserMenu menu) : base(null, menu) { }
        public override void Execute()
        {
            menu.ChangeActiveItem(ConsoleKey.LeftArrow);
        }
    }

    class UserMenuMoveCursorRightCommand : IUserMenuConnector
    {
        public UserMenuMoveCursorRightCommand(UserMenu menu) : base(null, menu) { }
        public override void Execute()
        {
            menu.ChangeActiveItem(ConsoleKey.RightArrow);
        }
    }

    class UserMenuMoveCursorHomeCommand : IUserMenuConnector
    {
        public UserMenuMoveCursorHomeCommand(UserMenu menu) : base(null, menu) { }
        public override void Execute()
        {
            menu.ChangeActiveItem(ConsoleKey.Home);
        }
    }

    class UserMenuMoveCursorEndCommand : IUserMenuConnector
    {
        public UserMenuMoveCursorEndCommand(UserMenu menu) : base(null, menu) { }
        public override void Execute()
        {
            menu.ChangeActiveItem(ConsoleKey.End);
        }
    }

    class UserMenuCloseCommand : IUserMenuConnector
    {
        public UserMenuCloseCommand(UserMenu menu) : base(null, menu) { }
        public override void Execute()
        {
            Program.ViewWithHotkeys = null;
        }
    }

    class UserMenuExecuteItemCommand : IUserMenuConnector
    {
        public UserMenuExecuteItemCommand(UserMenu menu) : base(null, menu) { }
        public override void Execute()
        {
            menu.ExecuteItem();
        }
    }
}
