﻿using FarPrototype.Common;
using FarPrototype.View;
using System;
using System.Collections.Generic;
using System.Text;

namespace FarPrototype.Connector
{
    class TypeBackspaceCommand : ITypingConnector
    {
        public TypeBackspaceCommand(IHasHotkeys area) : base(null, area) { }
        public override void Execute() => area.BackSpaceClick(false);
    }

    class TypeBackspaceCtrlCommand : ITypingConnector
    {
        public TypeBackspaceCtrlCommand(IHasHotkeys area) : base(null, area) { }
        public override void Execute() => area.BackSpaceClick(true);
    }

    class TypeDeleteCommand : ITypingConnector
    {
        public TypeDeleteCommand(IHasHotkeys area) : base(null, area) { }
        public override void Execute() => area.DeleteClick(false);
    }

    class TypeDeleteCtrlCommand : ITypingConnector
    {
        public TypeDeleteCtrlCommand(IHasHotkeys area) : base(null, area) { }
        public override void Execute() => area.DeleteClick(true);
    }

    class TypeHomeCommand : ITypingConnector
    {
        public TypeHomeCommand(IHasHotkeys area) : base(null, area) { }
        public override void Execute() => area.HomeClick();
    }

    class TypeEndCommand : ITypingConnector
    {
        public TypeEndCommand(IHasHotkeys area) : base(null, area) { }
        public override void Execute() => area.EndClick();
    }

    class TypeLeftArrowCommand : ITypingConnector
    {
        public TypeLeftArrowCommand(IHasHotkeys area) : base(null, area) { }
        public override void Execute() => area.ArrowClick(Arrow.Left, false);
    }

    class TypeLeftCtrlArrowCommand : ITypingConnector
    {
        public TypeLeftCtrlArrowCommand(IHasHotkeys area) : base(null, area) { }
        public override void Execute() => area.ArrowClick(Arrow.Left, true);
    }

    class TypeRightArrowCommand : ITypingConnector
    {
        public TypeRightArrowCommand(IHasHotkeys area) : base(null, area) { }
        public override void Execute() => area.ArrowClick(Arrow.Right, false);
    }

    class TypeRightCtrlArrowCommand : ITypingConnector
    {
        public TypeRightCtrlArrowCommand(IHasHotkeys area) : base(null, area) { }
        public override void Execute() => area.ArrowClick(Arrow.Right, true);
    }

    class TypeUpArrowCommand : ITypingConnector
    {
        public TypeUpArrowCommand(IHasHotkeys area) : base(null, area) { }
        public override void Execute() => area.ArrowClick(Arrow.Up, false);
    }

    class TypeUpCtrlArrowCommand : ITypingConnector
    {
        public TypeUpCtrlArrowCommand(IHasHotkeys area) : base(null, area) { }
        public override void Execute() => area.ArrowClick(Arrow.Up, true);
    }

    class TypeDownArrowCommand : ITypingConnector
    {
        public TypeDownArrowCommand(IHasHotkeys area) : base(null, area) { }
        public override void Execute() => area.ArrowClick(Arrow.Down, false);
    }

    class TypeDownCtrlArrowCommand : ITypingConnector
    {
        public TypeDownCtrlArrowCommand(IHasHotkeys area) : base(null, area) { }
        public override void Execute() => area.ArrowClick(Arrow.Down, true);
    }
}
