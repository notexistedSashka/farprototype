﻿using FarPrototype.Common;
using FarPrototype.View;
using FarPrototype.View.Areas;
using FarPrototype.View.Dialogs;
using FarPrototype.View.Dialogs.Controls;
using FarPrototype.View.Windows;
using System;
using System.Collections.Generic;
using System.Text;

namespace FarPrototype.Connector
{
    class SwapPannelCommand : IMainAreaConnector
    {
        public SwapPannelCommand(MainArea area) : base(null, area) { }
        public override void Execute()
        {
            area.SwapActivePannel();
        }
    }

    class OpenUserMenu : IMainAreaConnector
    {
        public OpenUserMenu(MainArea area) : base(null, area) { }
        public override void Execute()
        {
            Program.ViewWithHotkeys = new UserMenu();
        }
    }

    class ExitCommand : IMainAreaConnector
    {
        public ExitCommand(MainArea area) : base(null, area) { }
        public override void Execute()
        {
            Console.ResetColor();
            Environment.Exit(1);
        }
    }

    class EnterCommand : IMainAreaConnector
    {
        public EnterCommand(MainArea area) : base(null, area) { }
        public override void Execute()
        {
            // Lock with MainArea.ChangingPathsThread()
            if (Program.MainArea.CmdCommand == string.Empty)
            {
                lock (area)
                    area.ExecuteCurrentItem();
            }
            else
            {
                Program.Area = Program.CmdArea;
                Program.BackgroundArea = null;

                string command = Program.MainArea.CmdCommand;
                Program.MainArea.CmdCommand = string.Empty;
                Program.CmdArea.ExecuteCommand(command);

                Program.Area = Program.MainArea;
                Program.BackgroundArea = Program.CmdArea;
            }
        }
    }

    class CmdOpenCloseCommand : IMainAreaConnector
    {
        public CmdOpenCloseCommand(MainArea area) : base(null, area) { }
        public override void Execute()
        {
            Program.Area = Program.CmdArea;
            Program.BackgroundArea = null;
        }
    }

    class OpenDeleteDialogCommand : IMainAreaConnector
    {
        public OpenDeleteDialogCommand(MainArea area) : base(null, area) { }
        public override void Execute()
        {
            area.DeleteCurrentItem();
        }
    }

    class OpenCreateDirectoryDialogCommand : IMainAreaConnector
    {
        public OpenCreateDirectoryDialogCommand(MainArea area) : base(null, area) { }
        public override void Execute()
        {
            area.CreateNewDirectory();
        }
    }

    class OpenCreateFileDialogCommand : IMainAreaConnector
    {
        public OpenCreateFileDialogCommand(MainArea area) : base(null, area) { }
        public override void Execute()
        {
            area.CreateNewFile();
        }
    }

    class ResizePannelsToSmallerCommand : IMainAreaConnector
    {
        public ResizePannelsToSmallerCommand(MainArea area) : base(null, area) { }
        public override void Execute()
        {
            area.ResizePannels(Resize.ToSmaller, Pannel.Both);
        }
    }

    class ResizePannelsToBiggerCommand : IMainAreaConnector
    {
        public ResizePannelsToBiggerCommand(MainArea area) : base(null, area) { }
        public override void Execute()
        {
            area.ResizePannels(Resize.ToBigger, Pannel.Both);
        }
    }

    class ResizeLeftPannelToSmallerCommand : IMainAreaConnector
    {
        public ResizeLeftPannelToSmallerCommand(MainArea area) : base(null, area) { }
        public override void Execute()
        {
            area.ResizePannels(Resize.ToSmaller, Pannel.Left);
        }
    }

    class ResizeLeftPannelToBiggerCommand : IMainAreaConnector
    {
        public ResizeLeftPannelToBiggerCommand(MainArea area) : base(null, area) { }
        public override void Execute()
        {
            area.ResizePannels(Resize.ToBigger, Pannel.Left);
        }
    }


    class ResizeRightPannelToSmallerCommand : IMainAreaConnector
    {
        public ResizeRightPannelToSmallerCommand(MainArea area) : base(null, area) { }
        public override void Execute()
        {
            area.ResizePannels(Resize.ToSmaller, Pannel.Right);
        }
    }

    class ResizeRightPannelToBiggerCommand : IMainAreaConnector
    {
        public ResizeRightPannelToBiggerCommand(MainArea area) : base(null, area) { }
        public override void Execute()
        {
            area.ResizePannels(Resize.ToBigger, Pannel.Right);
        }
    }

    class HideRightPannelCommand : IMainAreaConnector
    {
        public HideRightPannelCommand(MainArea area) : base(null, area) { }
        public override void Execute()
        {
            area.HidePannel(Pannel.Right);
        }
    }

    class HideLeftPannelCommand : IMainAreaConnector
    {
        public HideLeftPannelCommand(MainArea area) : base(null, area) { }
        public override void Execute()
        {
            area.HidePannel(Pannel.Left);
        }
    }

    class MainAreaBackspacePressedCommand : IMainAreaConnector
    {
        public MainAreaBackspacePressedCommand(MainArea area) : base(null, area) { }
        public override void Execute()
        {
            if(area.CmdCommand != string.Empty)
                area.CmdCommand = area.CmdCommand.Substring(0, area.CmdCommand.Length - 1);
        }
    }

    class MainAreaClearCmdCommandCommand : IMainAreaConnector
    {
        public MainAreaClearCmdCommandCommand(MainArea area) : base(null, area) { }
        public override void Execute()
        {
            area.CmdCommand = string.Empty;
        }
    }

    class CopyCommand : IMainAreaConnector
    {
        public CopyCommand(MainArea area) : base(null, area) { }
        public override void Execute()
        {
            area.CopyReplace("Copy");
        }
    }

    class ReplaceCommand : IMainAreaConnector
    {
        public ReplaceCommand(MainArea area) : base(null, area) { }
        public override void Execute()
        {
            area.CopyReplace("Replace");
        }
    }

    class RenameCommand : IMainAreaConnector
    {
        public RenameCommand(MainArea area) : base(null, area) { }
        public override void Execute()
        {
            area.CopyReplace("Rename");
        }
    }

    class FindCommand : IMainAreaConnector
    {
        public FindCommand(MainArea area) : base(null, area) { }
        public override void Execute()
        {
            area.Find();
        }
    }
}
