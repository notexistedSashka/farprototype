﻿using FarPrototype.View;
using FarPrototype.View.Areas;
using FarPrototype.View.Dialogs.Controls;
using System;
using System.Collections.Generic;
using System.Text;

namespace FarPrototype.Connector
{
    public interface IParameterizedConnector
    {
        void Execute(string s);
    }

    abstract class IParameterizedTypeConnector : IParameterizedConnector
    {
        protected IHasHotkeys area;
        protected Model.Model model;

        public IParameterizedTypeConnector(Model.Model init_model, IHasHotkeys init_area)
        {
            model = init_model;
            area = init_area;
        }

        public abstract void Execute(string s);
    }
}
