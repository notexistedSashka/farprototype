﻿using FarPrototype.View.Dialogs.Controls;
using System;
using System.Collections.Generic;
using System.Text;

namespace FarPrototype.Connector
{
    class TextBoxUnselectToNextCommand : ITextBoxConnector
    {
        public TextBoxUnselectToNextCommand(TextBox textBox) : base(null, textBox) { }
        public override void Execute()
        {
            textBox.RedirectToParent(ConsoleKey.Tab.ToString() + "FalseFalseFalse");
        }
    }

    class TextBoxUnselectToPreviousCommand : ITextBoxConnector
    {
        public TextBoxUnselectToPreviousCommand(TextBox textBox) : base(null, textBox) { }
        public override void Execute()
        {
            textBox.RedirectToParent(ConsoleKey.Tab.ToString() + "FalseFalseTrue");
        }
    }

    class TextBoxEnterClickCommand : ITextBoxConnector
    {
        public TextBoxEnterClickCommand(TextBox textBox) : base(null, textBox) { }
        public override void Execute()
        {
            Program.ViewWithHotkeys = null;
            textBox.RedirectToParent(ConsoleKey.Enter.ToString() + "FalseFalseFalse");
        }
    }

    class TextBoxEscapeClickCommand : ITextBoxConnector
    {
        public TextBoxEscapeClickCommand(TextBox textBox) : base(null, textBox) { }
        public override void Execute()
        {
            textBox.RedirectToParent(ConsoleKey.Escape.ToString() + "FalseFalseFalse");
        }
    }
}
