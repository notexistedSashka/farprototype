﻿using FarPrototype.View.Areas;
using System;
using System.Collections.Generic;
using System.Text;

namespace FarPrototype.Connector
{
    class BackToPannelsCommand : ICmdAreaConnector
    {
        public BackToPannelsCommand(CmdArea area) : base(null, area) { }
        public override void Execute()
        {
            Program.Area = Program.MainArea;
            Program.BackgroundArea = null;
        }
    }

    class CmdExecuteCommand : ICmdAreaConnector
    {
        public CmdExecuteCommand(CmdArea area) : base(null, area) { }
        public override void Execute() => area.ExecuteCommand();
    }
}
