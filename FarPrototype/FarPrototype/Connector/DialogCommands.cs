﻿using FarPrototype.View.Dialogs;
using System;
using System.Collections.Generic;
using System.Text;

namespace FarPrototype.Connector
{
    class SelectNextControlCommand : IDialogConnector
    {
        public SelectNextControlCommand(Dialog area) : base(null, area) { }
        public override void Execute()
        {
            area.SelectNextControl();
        }
    }

    class SelectPreviousControlCommand : IDialogConnector
    {
        public SelectPreviousControlCommand(Dialog area) : base(null, area) { }
        public override void Execute()
        {
            area.SelectPreviousControl();
        }
    }

    class ExecuteControlCommand : IDialogConnector
    {
        public ExecuteControlCommand(Dialog area) : base(null, area) { }
        public override void Execute()
        {
            area.ExecuteControl();
        }
    }

    class ExecuteCancelCommand : IDialogConnector
    {
        public ExecuteCancelCommand(Dialog area) : base(null, area) { }
        public override void Execute()
        {
            Program.Area = Program.MainArea;
            Program.BackgroundArea = Program.CmdArea;
            Program.ViewWithHotkeys = null;
        }
    }
}
