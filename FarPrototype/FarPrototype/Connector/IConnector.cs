﻿using FarPrototype.View.Areas;
using FarPrototype.Model;
using FarPrototype.View.Dialogs;
using FarPrototype.View.Windows;
using FarPrototype.View.Dialogs.Controls;
using FarPrototype.View;

namespace FarPrototype.Connector
{
    public interface IConnector
    {
        void Execute();
    }

    abstract class IMainAreaConnector : IConnector
    {
        protected MainArea area;
        protected Model.Model model;

        public IMainAreaConnector(Model.Model init_model, MainArea init_area)
        {
            model = init_model;
            area = init_area;
        }

        public abstract void Execute();
    }

    abstract class IDialogConnector : IConnector
    {
        protected Dialog area;
        protected Model.Model model;

        public IDialogConnector(Model.Model init_model, Dialog init_area)
        {
            model = init_model;
            area = init_area;
        }

        public abstract void Execute();
    }

    abstract class ICmdAreaConnector : IConnector
    {
        protected CmdArea area;
        protected Model.Model model;

        public ICmdAreaConnector(Model.Model init_model, CmdArea init_area)
        {
            model = init_model;
            area = init_area;
        }

        public abstract void Execute();
    }

    abstract class IUserMenuConnector : IConnector
    {
        protected UserMenu menu;
        protected Model.Model model;

        public IUserMenuConnector(Model.Model init_model, UserMenu init_menu)
        {
            model = init_model;
            menu = init_menu;
        }

        public abstract void Execute();
    }

    abstract class ITextBoxConnector : IConnector
    {
        protected TextBox textBox;
        protected Model.Model model;

        public ITextBoxConnector(Model.Model init_model, TextBox init_textBox)
        {
            model = init_model;
            textBox = init_textBox;
        }

        public abstract void Execute();
    }

    abstract class ICheckBoxConnector : IConnector
    {
        protected CheckBox checkBox;
        protected Model.Model model;

        public ICheckBoxConnector(Model.Model init_model, CheckBox init_checkBox)
        {
            model = init_model;
            checkBox = init_checkBox;
        }

        public abstract void Execute();
    }

    abstract class ITypingConnector : IConnector
    {
        protected IHasHotkeys area;
        protected Model.Model model;

        public ITypingConnector(Model.Model init_model, IHasHotkeys init_area)
        {
            model = init_model;
            area = init_area;
        }

        public abstract void Execute();
    }
}
