﻿using FarPrototype.View;
using FarPrototype.View.Areas;

namespace FarPrototype.Connector
{
    class TypingSymbolPressedCommand : IParameterizedTypeConnector
    {
        public TypingSymbolPressedCommand(IHasHotkeys area) : base(null, area) { }
        public override void Execute(string s)
        {
            area.TypeSymbol(s);
        }
    }
}
