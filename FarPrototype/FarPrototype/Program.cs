﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using FarPrototype.Model;
using FarPrototype.View;
using FarPrototype.View.Areas;
using FarPrototype.View.Windows;

namespace FarPrototype
{
    class Program
    {
        public static MainArea MainArea { get; set; }
        public static CmdArea CmdArea { get; set; }

        private static IArea area;
        public static IArea BackgroundArea { get; set; }
        public static IArea Area
        {
            get
            {
                return area;
            }
            set
            {
                area = value;
                if (value != null)
                {
                    area.ResizeWindows();
                    area.Show();
                    Console.ResetColor();
                }
            }
        }

        private static IHasHotkeys viewWithHotkeys;
        public static IHasHotkeys ViewWithHotkeys
        {
            get
            {
                return viewWithHotkeys;
            }
            set
            {
                if (value == null)
                {
                    area.Show();
                    Console.ResetColor();
                }

                viewWithHotkeys = value;

                if(viewWithHotkeys != null)
                    viewWithHotkeys.Show();
            }
        }

        private static int windowWidth = Console.LargestWindowWidth;
        private static int windowHeight = Console.LargestWindowHeight;

        private const uint ENABLE_QUICK_EDIT = 0x0040;
        private const uint ENABLE_PROCESSED_INPUT = 0x0001;
        private const int STD_INPUT_HANDLE = -10;
        private const int STD_OUTPUT_HANDLE = -11;

        //[DllImport("user32.dll", EntryPoint = "SetWindowPos")]
        //public static extern IntPtr SetWindowPos(IntPtr hWnd, int hWndInsertAfter, int x, int Y, int cx, int cy, int wFlags);

        [DllImport("user32.dll")]
        public static extern bool ShowWindow(IntPtr hConsoleHandle, int cmdShow);

        [DllImport("kernel32.dll")]
        private static extern bool GetConsoleMode(IntPtr hConsoleHandle, out uint lpMode);

        [DllImport("kernel32.dll")]
        static extern IntPtr GetConsoleWindow();

        [DllImport("kernel32.dll")]
        private static extern bool SetConsoleMode(IntPtr hConsoleHandle, uint dwMode);

        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern IntPtr GetStdHandle(int nStdHandle);

        // TODO: make structures fot position coords and sizes
        static void Main(string[] args)
        {
            // Set window size and mode
            Console.Title = "FarPrototype";
            var hConsoleHandle = GetConsoleWindow();
            //var SWP_NOSIZE = 0x1;
            //var HWND_TOPMOST = 1;
            //SetWindowPos(hWnd, HWND_TOPMOST, -8, 0, 0, 0, SWP_NOSIZE);
            ShowWindow(hConsoleHandle, 3);

            var inHandle = GetStdHandle(STD_INPUT_HANDLE);

            if (!GetConsoleMode(inHandle, out uint consoleMode))
            {
                Console.WriteLine("failed to get input console mode");
                return;
            }

            // Disable mouse
            consoleMode &= ~ENABLE_QUICK_EDIT;
            // Disable all console hotcuts
            consoleMode &= ~ENABLE_PROCESSED_INPUT;
            SetConsoleMode(inHandle, consoleMode);

            // Start console setting
            Console.SetWindowSize(windowWidth, windowHeight);
            Console.SetBufferSize(Console.WindowWidth, Console.WindowHeight);

            var consoleMini = new ConsoleMiniWindow(new Position(0, Console.WindowHeight - 3), new Size(Console.WindowWidth, 1), string.Empty);
            MainArea = new MainArea(consoleMini);
            CmdArea = new CmdArea(consoleMini);

            Area = MainArea;

            var thread = new Thread(new ThreadStart(ChangingWindowsSizeThread));
            thread.Start();

            while (true)
            {
                if (Console.KeyAvailable)
                {
                    var key = Console.ReadKey(true);

                    string commandKey = key.Key.ToString()
                        + key.Modifiers.HasFlag(ConsoleModifiers.Alt)
                        + key.Modifiers.HasFlag(ConsoleModifiers.Control)
                        + key.Modifiers.HasFlag(ConsoleModifiers.Shift);

                    try
                    {
                        if (ViewWithHotkeys == null)
                            area.Commands[commandKey].Execute();
                        else
                            ViewWithHotkeys.Commands[commandKey].Execute();
                    }
                    catch (KeyNotFoundException)
                    {
                        commandKey = key.KeyChar.ToString()
                            + key.Modifiers.HasFlag(ConsoleModifiers.Alt)
                            + key.Modifiers.HasFlag(ConsoleModifiers.Control)
                            + key.Modifiers.HasFlag(ConsoleModifiers.Shift);

                        try
                        {
                            if (ViewWithHotkeys == null)
                                area.ParameterizedCommands[commandKey].Execute(key.KeyChar.ToString());
                            else
                                ViewWithHotkeys.ParameterizedCommands[commandKey].Execute(key.KeyChar.ToString());
                        }
                        catch (KeyNotFoundException) { }
                    }

                    Console.ResetColor();

                    // Clear buffer
                    while (Console.KeyAvailable) { Console.ReadKey(true); }
                }
            }
        }

        private static void ChangingWindowsSizeThread()
        {
            // TODO: resize buffer
            while (true)
            {
                Thread.Sleep(50);
                if ((Console.WindowWidth != windowWidth || Console.WindowHeight != windowHeight)
                    && Console.WindowWidth > Common.CommonData.minWidth && Console.WindowHeight > Common.CommonData.minHeight)
                {
                    windowWidth = Console.WindowWidth;
                    windowHeight = Console.WindowHeight;

                    Console.CursorVisible = false;
                    Console.ResetColor();
                    Console.Clear();
                    if(BackgroundArea != null)
                    {
                        BackgroundArea.ResizeWindows();
                        BackgroundArea.Show();
                    }
                    area.ResizeWindows();
                    area.Show();
                    Console.ResetColor();
                }
            }
        }
    }

}
