﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FarPrototype.View
{
    public struct Position
    {
        public int PositionX;
        public int PositionY;

        public Position(int posX, int posY)
        {
            PositionX = posX;
            PositionY = posY;
        }
    }

    public struct Size
    {
        public int Width;
        public int Height;

        public Size(int width, int height)
        {
            Width = width;
            Height = height;
        }
    }

    public struct FrontBackColor
    {
        public ConsoleColor Front;
        public ConsoleColor Back;

        public FrontBackColor(ConsoleColor front, ConsoleColor back)
        {
            Front = front;
            Back = back;
        }
    }
}
