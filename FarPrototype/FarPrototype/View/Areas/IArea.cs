﻿using FarPrototype.Common;
using FarPrototype.Connector;
using FarPrototype.View.Windows;
using System;
using System.Collections.Generic;
using System.Text;

namespace FarPrototype.View.Areas
{
    public interface IArea : IHasHotkeys
    {
        void ResizeWindows();
    }
}
