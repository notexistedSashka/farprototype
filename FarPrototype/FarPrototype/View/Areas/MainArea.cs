﻿using FarPrototype.Common;
using FarPrototype.Connector;
using FarPrototype.View.Windows;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading;
using System.Linq;
using System.Security.Permissions;
using FarPrototype.View.Dialogs;
using System.Reflection;

namespace FarPrototype.View.Areas
{
    class MainArea : IArea
    {
        public List<IWindow> Windows { get; set; }

        private PannelWindow leftPannel;
        private PannelWindow rightPannel;
        private ConsoleMiniWindow consoleMini;
        private BottomControlsWindow bottomControls;
        private UserMenu userMenu;

        private RegistryKey key;
        private string leftPath;
        private string rightPath;
        public string CmdCommand
        {
            get => consoleMini.CmdCommand;
            set => consoleMini.CmdCommand = value;
        }

        public Dictionary<string, IConnector> Commands { get; set; }
        public Dictionary<string, IParameterizedConnector> ParameterizedCommands { get; set; }

        public MainArea(ConsoleMiniWindow consoleMini)
        {
            key = Registry.CurrentUser.GetSubKeyNames().Contains(@"Software\Sasha Pachev\FarPrototype") ?
                Registry.CurrentUser.OpenSubKey(@"Software\Sasha Pachev\FarPrototype") :
                Registry.CurrentUser.CreateSubKey(@"Software\Sasha Pachev\FarPrototype");

            CreateCommands();

            this.consoleMini = consoleMini;
            CreateWindows();

            var thread = new Thread(new ThreadStart(ChangingPathsThread));
            thread.Start();

            CmdCommand = string.Empty;
        }

        private void CreateCommands()
        {
            Commands = new Dictionary<string, IConnector>();

            // IHasHotkeys
            Commands.Add(ConsoleKey.Backspace.ToString() + "FalseFalseFalse", new TypeBackspaceCommand(this));
            Commands.Add(ConsoleKey.Backspace.ToString() + "FalseTrueFalse", new TypeBackspaceCtrlCommand(this));
            Commands.Add(ConsoleKey.Delete.ToString() + "FalseFalseFalse", new TypeDeleteCommand(this));
            Commands.Add(ConsoleKey.Delete.ToString() + "FalseTrueFalse", new TypeDeleteCtrlCommand(this));

            Commands.Add(ConsoleKey.Home.ToString() + "FalseFalseFalse", new TypeHomeCommand(this));
            Commands.Add(ConsoleKey.End.ToString() + "FalseFalseFalse", new TypeEndCommand(this));
            Commands.Add(ConsoleKey.UpArrow.ToString() + "FalseFalseFalse", new TypeUpArrowCommand(this));
            Commands.Add(ConsoleKey.DownArrow.ToString() + "FalseFalseFalse", new TypeDownArrowCommand(this));
            Commands.Add(ConsoleKey.RightArrow.ToString() + "FalseFalseFalse", new TypeRightArrowCommand(this));
            Commands.Add(ConsoleKey.LeftArrow.ToString() + "FalseFalseFalse", new TypeLeftArrowCommand(this));

            // Swap pannel
            Commands.Add(ConsoleKey.Tab.ToString() + "FalseFalseFalse", new SwapPannelCommand(this));

            // Execute selected item
            Commands.Add(ConsoleKey.Enter.ToString() + "FalseFalseFalse", new EnterCommand(this));

            // Exit
            Commands.Add(ConsoleKey.F10.ToString() + "FalseFalseFalse", new ExitCommand(this));

            // User menu
            Commands.Add(ConsoleKey.F2.ToString() + "FalseFalseFalse", new OpenUserMenu(this));

            // Swap to CmdArea
            Commands.Add(ConsoleKey.O.ToString() + "FalseTrueFalse", new CmdOpenCloseCommand(this));

            // Deleting
            Commands.Add(ConsoleKey.F8.ToString() + "FalseFalseFalse", new OpenDeleteDialogCommand(this));

            // Creating
            Commands.Add(ConsoleKey.F7.ToString() + "FalseFalseFalse", new OpenCreateDirectoryDialogCommand(this));
            Commands.Add(ConsoleKey.F4.ToString() + "FalseFalseTrue", new OpenCreateFileDialogCommand(this));

            // Search
            Commands.Add(ConsoleKey.F7.ToString() + "TrueFalseFalse", new FindCommand(this));

            // Copy/Replace
            Commands.Add(ConsoleKey.F5.ToString() + "FalseFalseFalse", new CopyCommand(this));
            Commands.Add(ConsoleKey.F6.ToString() + "FalseFalseFalse", new ReplaceCommand(this));
            Commands.Add(ConsoleKey.F6.ToString() + "FalseFalseTrue", new RenameCommand(this));

            // Resize pannels
            Commands.Add(ConsoleKey.B.ToString() + "FalseTrueFalse", new ResizePannelsToSmallerCommand(this));
            Commands.Add(ConsoleKey.B.ToString() + "TrueFalseFalse", new ResizePannelsToBiggerCommand(this));
            Commands.Add(ConsoleKey.L.ToString() + "FalseTrueFalse", new ResizeLeftPannelToSmallerCommand(this));
            Commands.Add(ConsoleKey.L.ToString() + "TrueFalseFalse", new ResizeLeftPannelToBiggerCommand(this));
            Commands.Add(ConsoleKey.R.ToString() + "FalseTrueFalse", new ResizeRightPannelToSmallerCommand(this));
            Commands.Add(ConsoleKey.R.ToString() + "TrueFalseFalse", new ResizeRightPannelToBiggerCommand(this));

            // Hide pannels
            Commands.Add(ConsoleKey.F1.ToString() + "FalseTrueFalse", new HideLeftPannelCommand(this));
            Commands.Add(ConsoleKey.F2.ToString() + "FalseTrueFalse", new HideRightPannelCommand(this));

            ParameterizedCommands = new Dictionary<string, IParameterizedConnector>();

            for (var i = 0; i < 2048; i++) // Set commands for most chars with shift and without
            {
                char ch = Convert.ToChar(i);

                if (char.IsLetterOrDigit(ch) || char.IsPunctuation(ch) || char.IsWhiteSpace(ch))
                {
                    ParameterizedCommands.Add(ch + "FalseFalseFalse", new TypingSymbolPressedCommand(this));
                    ParameterizedCommands.Add(ch + "FalseFalseTrue", new TypingSymbolPressedCommand(this));
                }
            }
        }

        private void CreatePannels()
        {
            string defaultPath = Directory.GetCurrentDirectory();

            try
            {
                leftPath = key.GetValue("LeftPath").ToString();

                if(!Directory.Exists(leftPath))
                    leftPath = defaultPath;
            }
            catch (Exception)
            { leftPath = defaultPath; }

            try
            {
                rightPath = key.GetValue("RightPath").ToString();

                if (!Directory.Exists(rightPath))
                    rightPath = defaultPath;
            }
            catch (Exception) { rightPath = defaultPath; }

            leftPannel = new PannelWindow(new Position(0, 0), new Size(Console.WindowWidth / 2, Console.WindowHeight - 3), leftPath);
            rightPannel = new PannelWindow(new Position(Console.WindowWidth / 2, 0), new Size(Console.WindowWidth / 2, Console.WindowHeight - 3), rightPath);

            Windows.Add(leftPannel);
            Windows.Add(rightPannel);

            leftPannel.IsActive = true;
            consoleMini.DirectoryPath = leftPannel.DirectoryPath;
        }

        public void ResizePannels(Resize resize, Pannel pannel)
        {
            switch (resize)
            {
                case Resize.ToBigger:
                    if(leftPannel.Height < Console.WindowHeight - 3 && (pannel == Pannel.Both || pannel == Pannel.Left))
                        leftPannel.SetSize(leftPannel.Width, leftPannel.Height + 1);
                    if (rightPannel.Height < Console.WindowHeight - 3 && (pannel == Pannel.Both || pannel == Pannel.Right))
                        rightPannel.SetSize(rightPannel.Width, rightPannel.Height + 1);
                    break;
                case Resize.ToSmaller:
                    if (leftPannel.Height > 7 && (pannel == Pannel.Both || pannel == Pannel.Left))
                        leftPannel.SetSize(leftPannel.Width, leftPannel.Height - 1);
                    if (rightPannel.Height > 7 && (pannel == Pannel.Both || pannel == Pannel.Right))
                        rightPannel.SetSize(rightPannel.Width, rightPannel.Height - 1);
                    break;
            }

            Console.CursorVisible = false;
            Program.CmdArea.Show();
            leftPannel.Show();
            rightPannel.Show();
        }

        public void HidePannel(Pannel pannel)
        {
            switch (pannel)
            {
                case Pannel.Left:
                    leftPannel.IsHidden = !leftPannel.IsHidden;

                    rightPannel.IsActive = true;
                    leftPannel.IsActive = false;
                    break;
                case Pannel.Right:
                    rightPannel.IsHidden = !rightPannel.IsHidden;

                    leftPannel.IsActive = true;
                    rightPannel.IsActive = false;
                    break;
            }

            Console.CursorVisible = false;
            Program.CmdArea.Show();

            leftPannel.Show();
            rightPannel.Show();
        }

        public void ResizeWindows()
        {
            consoleMini.SetSize(Console.WindowWidth, 1);
            leftPannel.SetSize(Console.WindowWidth / 2, Console.WindowHeight - 3);
            rightPannel.SetSize(Console.WindowWidth / 2, Console.WindowHeight - 3);
            bottomControls.SetSize(Console.WindowWidth, 1);

            leftPannel.SetPosition(0, 0);
            rightPannel.SetPosition(Console.WindowWidth / 2, 0);
            consoleMini.SetPosition(0, Console.WindowHeight - 3);
            bottomControls.SetPosition(0, Console.WindowHeight - 2);
        }

        private void CreateWindows()
        {
            Windows = new List<IWindow>();

            CreatePannels();

            bottomControls = new BottomControlsWindow(new Position(0, Console.WindowHeight - 2), new Size(Console.WindowWidth, 1));

            Windows.Add(consoleMini);
            Windows.Add(bottomControls);
        }

        public void Show()
        {
            // TODO: replace this line
            SetDirectoryPath(consoleMini.DirectoryPath);

            foreach (var window in Windows)
                window.Show();
            Console.SetCursorPosition(CommonData.cursorCoords.Item1, CommonData.cursorCoords.Item2);
            Console.CursorVisible = true;
        }

        public void SwapActivePannel()
        {
            if (!leftPannel.IsHidden && !rightPannel.IsHidden)
            {
                leftPannel.IsActive = !leftPannel.IsActive;
                rightPannel.IsActive = !rightPannel.IsActive;

                consoleMini.DirectoryPath = leftPannel.IsActive ? leftPannel.DirectoryPath : rightPannel.DirectoryPath;

                leftPannel.ShowItems();
                rightPannel.ShowItems();
            }
        }

        public void MovePannelCursor(ConsoleKey key)
        {
            if (leftPannel.IsActive)
                leftPannel.ChangeActiveItem(key);
            else
                rightPannel.ChangeActiveItem(key);
        }

        public void ExecuteCurrentItem()
        {
            var pannel = leftPannel.IsActive ? leftPannel : rightPannel;

            string prevPath = pannel.DirectoryPath;
            string item = Path.GetFullPath(Path.Combine(pannel.DirectoryPath, pannel.CurrentItem).Trim());

            var info = new FileInfo(item);

            try
            {
                if (Directory.Exists(item))
                {
                    try
                    {
                        pannel.DirectoryPath = item;
                        pannel.UpdateForNewPathPannel();
                        pannel.ShowItems();
                    }
                    catch (Exception)
                    {
                        pannel.DirectoryPath = prevPath;
                        throw;
                    }
                }
                else
                {
                    var proc = new Process();
                    proc.StartInfo.FileName = item;
                    proc.StartInfo.UseShellExecute = true;
                    proc.Start();
                }
            }
            //catch (UnauthorizedAccessException)
            //{
            //    ProcessStartInfo proc = new ProcessStartInfo();
            //    proc.UseShellExecute = true;
            //    proc.WorkingDirectory = Environment.CurrentDirectory;
            //    proc.FileName = Path.ChangeExtension(Assembly.GetExecutingAssembly().Location, "exe");
            //    proc.Verb = "runas";
            //    try
            //    {
            //        if (!Model.Model.IsRunAsAdmin())
            //        {
            //            Process.Start(proc);

            //            Console.ResetColor();
            //            Environment.Exit(1);
            //        }
            //    }
            //    catch (Exception ex)
            //    {
            //        int width = ex.Message.Length + 4;
            //        var size = new Size(width, 5);
            //        var dialog = new ErrorDialog(size, OnCancelEvent, ex);

            //        Program.Area = dialog;
            //        Program.BackgroundArea = this;
            //    }
            //}
            catch (Exception ex)
            {
                int width = ex.Message.Length + 4;
                var size = new Size(width, 5);
                var dialog = new ErrorDialog(size, OnCancelEvent, ex);

                Program.Area = dialog;
                Program.BackgroundArea = this;
            }
        }

        private void ChangingPathsThread()
        {
            while (true)
            {
                if(leftPannel.DirectoryPath != leftPath)
                {
                    leftPath = leftPannel.DirectoryPath;
                    // Lock MoveCursorEnterCommand.Execute()
                    lock (this)
                        consoleMini.DirectoryPath = leftPannel.DirectoryPath;

                    key.SetValue("LeftPath", leftPath);
                }
                if(rightPannel.DirectoryPath != rightPath)
                {
                    rightPath = rightPannel.DirectoryPath;
                    lock (this)
                        consoleMini.DirectoryPath = rightPannel.DirectoryPath;

                    key.SetValue("RightPath", rightPath);
                }
            }
        }

        public void SetDirectoryPath(string path)
        {
            // TODO: fix showing
            var pannel = leftPannel.IsActive ? leftPannel : rightPannel;
            pannel.DirectoryPath = path;
            Thread.Sleep(5);
        }

        public void DeleteCurrentItem()
        {
            var pannel = leftPannel.IsActive ? leftPannel : rightPannel;

            if (pannel.CurrentItem != ".." && !pannel.IsHidden)
            {
                string fileName = Path.Combine(pannel.DirectoryPath, pannel.CurrentItem);
                int width = fileName.Length + 4;

                if (width > Console.WindowWidth - 6)
                    width = Console.WindowWidth - 6;
                else if (width < 30)
                    width = 30;

                var size = new Size(width, 6);
                var dialog = new DeleteDialog(size, fileName);

                Program.Area = dialog;
                Program.BackgroundArea = Program.MainArea;
            }
        }

        public void CreateNewDirectory()
        {
            var pannel = leftPannel.IsActive ? leftPannel : rightPannel;

            if(!pannel.IsHidden)
            {
                var dialog = new CreateDirectoryDialog(pannel.DirectoryPath);

                Program.Area = dialog;
                Program.BackgroundArea = Program.MainArea;
            }
        }

        public void CreateNewFile()
        {
            var pannel = leftPannel.IsActive ? leftPannel : rightPannel;

            if (!pannel.IsHidden)
            {
                var dialog = new CreateFileDialog(pannel.DirectoryPath);

                Program.Area = dialog;
                Program.BackgroundArea = Program.MainArea;
            }
        }

        public void CopyReplace(string command)
        {
            var pannelMain = leftPannel.IsActive ? leftPannel : rightPannel;
            var pannelSecondary = !leftPannel.IsActive ? leftPannel : rightPannel;

            string destPath = !pannelSecondary.IsHidden ? pannelSecondary.DirectoryPath : pannelMain.DirectoryPath;

            if (pannelMain.CurrentItem != ".." && !pannelMain.IsHidden)
            {
                Dialog dialog = new ErrorDialog(new Size(20, 5), OnCancelEvent, "No command");

                switch (command)
                {
                    case "Copy":
                        dialog = new CopyReplaceDialog(Path.Combine(pannelMain.DirectoryPath, pannelMain.CurrentItem), destPath, command);
                        break;
                    case "Replace":
                        dialog = new CopyReplaceDialog(Path.Combine(pannelMain.DirectoryPath, pannelMain.CurrentItem), destPath, command);
                        break;
                    case "Rename":
                        dialog = new CopyReplaceDialog(Path.Combine(pannelMain.DirectoryPath, pannelMain.CurrentItem), pannelMain.CurrentItem, command);
                        break;
                }

                Program.Area = dialog;
                Program.BackgroundArea = Program.MainArea;
            }
        }

        public void Find()
        {
            var pannel = leftPannel.IsActive ? leftPannel : rightPannel;

            if (!pannel.IsHidden)
            {
                var dialog = new FindDialog(pannel.DirectoryPath);

                Program.Area = dialog;
                Program.BackgroundArea = Program.MainArea;
            }
        }

        public void BackSpaceClick(bool withCtrl)
        {
            string command = consoleMini.CmdCommand;
            int cursorPosition = consoleMini.CursorPosition;

            if (withCtrl)
            {
                if (cursorPosition < command.Length)
                    command = command.Substring(cursorPosition, command.Length - cursorPosition);
                else
                    command = string.Empty;
                cursorPosition = 0;
                Show();
            }
            else
            {
                if (command != string.Empty && cursorPosition > 0)
                {
                    cursorPosition--;
                    command = command.Remove(cursorPosition, 1);
                }
            }

            consoleMini.CmdCommand = command;
            consoleMini.CursorPosition = cursorPosition;
        }

        public void EndClick()
        {
            MovePannelCursor(ConsoleKey.End);
        }

        public void HomeClick()
        {
            MovePannelCursor(ConsoleKey.Home);
        }

        public void DeleteClick(bool withCtrl)
        {
            string command = consoleMini.CmdCommand;
            int cursorPosition = consoleMini.CursorPosition;

            if (withCtrl)
            {
                if (cursorPosition > 0)
                    command = command.Substring(0, cursorPosition);
                else
                    command = string.Empty;
                Show();
            }
            else
            {
                if (command != string.Empty && cursorPosition < command.Length)
                    command = command.Remove(cursorPosition, 1);
            }

            consoleMini.CmdCommand = command;
            consoleMini.CursorPosition = cursorPosition;
        }

        public void ArrowClick(Arrow arrow, bool withCtrl)
        {
            switch (arrow)
            {
                case Arrow.Left:
                    MovePannelCursor(ConsoleKey.LeftArrow);
                    break;
                case Arrow.Right:
                    MovePannelCursor(ConsoleKey.RightArrow);
                    break;
                case Arrow.Up:
                    MovePannelCursor(ConsoleKey.UpArrow);
                    break;
                case Arrow.Down:
                    MovePannelCursor(ConsoleKey.DownArrow);
                    break;
            }
        }

        public void TypeSymbol(string s)
        {
            consoleMini.CmdCommand = consoleMini.CmdCommand.Insert(consoleMini.CursorPosition, s);
            consoleMini.CursorPosition++;
        }

        // Events
        // ------
        private void OnCancelEvent(object sender, EventArgs e)
        {
            Program.Area = Program.MainArea;
            Program.BackgroundArea = null;
        }
    }
}
