﻿using FarPrototype.Common;
using FarPrototype.Connector;
using FarPrototype.View.Windows;
using System;
using System.Collections.Generic;
using System.Text;

namespace FarPrototype.View.Areas
{
    class CmdArea : IArea
    {
        public Dictionary<string, IConnector> Commands { get; set; }
        public Dictionary<string, IParameterizedConnector> ParameterizedCommands { get; set; }

        public List<IWindow> Windows { get; set; }

        private BottomControlsWindow bottomControls;
        private CmdWindow cmd;
        private ConsoleMiniWindow consoleMini;

        // TODO: delete
        public string CmdCommand
        {
            get => consoleMini.CmdCommand;
            set => consoleMini.CmdCommand = value;
        }

        public CmdArea(ConsoleMiniWindow consoleMini)
        {
            CreateCommands();

            this.consoleMini = consoleMini;
            CreateWindows();
        }

        private void CreateCommands()
        {
            Commands = new Dictionary<string, IConnector>();
            Commands.Add(ConsoleKey.O.ToString() + "FalseTrueFalse", new BackToPannelsCommand(this));

            // IHasHotkeys
            Commands.Add(ConsoleKey.Backspace.ToString() + "FalseFalseFalse", new TypeBackspaceCommand(this));
            Commands.Add(ConsoleKey.Backspace.ToString() + "FalseTrueFalse", new TypeBackspaceCtrlCommand(this));
            Commands.Add(ConsoleKey.Delete.ToString() + "FalseFalseFalse", new TypeDeleteCommand(this));
            Commands.Add(ConsoleKey.Delete.ToString() + "FalseTrueFalse", new TypeDeleteCtrlCommand(this));

            Commands.Add(ConsoleKey.Home.ToString() + "FalseFalseFalse", new TypeHomeCommand(this));
            Commands.Add(ConsoleKey.End.ToString() + "FalseFalseFalse", new TypeEndCommand(this));
            Commands.Add(ConsoleKey.UpArrow.ToString() + "FalseFalseFalse", new TypeUpArrowCommand(this));
            Commands.Add(ConsoleKey.DownArrow.ToString() + "FalseFalseFalse", new TypeDownArrowCommand(this));
            Commands.Add(ConsoleKey.RightArrow.ToString() + "FalseFalseFalse", new TypeRightArrowCommand(this));
            Commands.Add(ConsoleKey.LeftArrow.ToString() + "FalseFalseFalse", new TypeLeftArrowCommand(this));

            // Exit
            Commands.Add(ConsoleKey.F10.ToString() + "FalseFalseFalse", new ExitCommand(Program.MainArea));

            // Execute
            Commands.Add(ConsoleKey.Enter.ToString() + "FalseFalseFalse", new CmdExecuteCommand(this));

            ParameterizedCommands = new Dictionary<string, IParameterizedConnector>();

            for (var i = 0; i < 2048; i++) // Set commands for most chars with shift and without
            {
                char ch = Convert.ToChar(i);

                if (char.IsLetterOrDigit(ch) || char.IsPunctuation(ch) || char.IsWhiteSpace(ch))
                {
                    ParameterizedCommands.Add(ch + "FalseFalseFalse", new TypingSymbolPressedCommand(this));
                    ParameterizedCommands.Add(ch + "FalseFalseTrue", new TypingSymbolPressedCommand(this));
                }
            }
        }

        public void ResizeWindows()
        {
            consoleMini.SetPosition(0, Console.WindowHeight - 3);
            cmd.SetPosition(0, 0);
            bottomControls.SetPosition(0, Console.WindowHeight - 2);

            consoleMini.SetSize(Console.WindowWidth, Console.WindowHeight - 3);
            cmd.SetSize(Console.WindowWidth, Console.WindowHeight - 3);
            bottomControls.SetSize(Console.WindowWidth, 1);
        }

        public void CreateWindows()
        {
            Windows = new List<IWindow>();

            cmd = new CmdWindow(new Position(0, 0), new Size(Console.WindowWidth, Console.WindowHeight - 3));
            bottomControls = new BottomControlsWindow(new Position(0, Console.WindowHeight - 2), new Size(Console.WindowWidth, 1));

            Windows.Add(bottomControls);
            Windows.Add(consoleMini);
            Windows.Add(cmd);
        }

        public void ExecuteCommand() => consoleMini.ExecutingUserCommand(cmd);

        public void ExecuteCommand(string command) => consoleMini.ExecutingUserCommand(cmd, command);

        public void Show()
        {
            Console.Clear();
            foreach (var window in Windows)
                window.Show();
        }

        public void BackSpaceClick(bool withCtrl)
        {
            string command = consoleMini.CmdCommand;
            int cursorPosition = consoleMini.CursorPosition;

            if (withCtrl)
            {
                if (cursorPosition < command.Length)
                    command = command.Substring(cursorPosition, command.Length - cursorPosition);
                else
                    command = string.Empty;
                cursorPosition = 0;
                Show();
            }
            else
            {
                if (command != string.Empty && cursorPosition > 0)
                {
                    cursorPosition--;
                    command = command.Remove(cursorPosition, 1);
                }
            }

            consoleMini.CmdCommand = command;
            consoleMini.CursorPosition = cursorPosition;
        }

        public void EndClick()
        {
            consoleMini.CursorPosition = consoleMini.CmdCommand.Length;
        }

        public void HomeClick()
        {
            consoleMini.CursorPosition = 0;
        }

        public void DeleteClick(bool withCtrl)
        {
            string command = consoleMini.CmdCommand;
            int cursorPosition = consoleMini.CursorPosition;

            if (withCtrl)
            {
                if (cursorPosition > 0)
                    command = command.Substring(0, cursorPosition);
                else
                    command = string.Empty;
                Show();
            }
            else
            {
                if (command != string.Empty && cursorPosition < command.Length)
                    command = command.Remove(cursorPosition, 1);
            }

            consoleMini.CmdCommand = command;
            consoleMini.CursorPosition = cursorPosition;
        }

        public void ArrowClick(Arrow arrow, bool withCtrl)
        {
            switch (arrow)
            {
                case Arrow.Left:
                    if (consoleMini.CursorPosition > 0)
                        consoleMini.CursorPosition--;
                    break;
                case Arrow.Right:
                    if (consoleMini.CursorPosition < consoleMini.CmdCommand.Length)
                        consoleMini.CursorPosition++;
                    break;
            }
        }

        public void TypeSymbol(string s)
        {
            consoleMini.CmdCommand = consoleMini.CmdCommand.Insert(consoleMini.CursorPosition, s);
            consoleMini.CursorPosition++;
        }
    }
}
