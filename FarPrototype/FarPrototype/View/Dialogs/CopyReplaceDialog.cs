﻿using FarPrototype.View.Dialogs.Controls;
using System;
using System.Collections.Generic;
using System.Text;

namespace FarPrototype.View.Dialogs
{
    class CopyReplaceDialog : Dialog
    {
        private string srcDirectoryPath = string.Empty;
        private string destDirectoryPath = string.Empty;

        private string title;

        private TextBox textBox;

        public CopyReplaceDialog(string srcDirectoryPath, string destDirectoryPath, string title)
            : base(new Size(35, 6), new FrontBackColor(ConsoleColor.Gray, ConsoleColor.DarkBlue), title)
        {
            this.srcDirectoryPath = srcDirectoryPath;
            this.destDirectoryPath = destDirectoryPath;
            this.title = title;

            Build();
        }

        protected override void Build()
        {
            Controls.Add(new Label(new Position(1, 1), new Size(title.Length +  5, 1), this, $"{title} to: "));

            textBox = new TextBox(new Position(1, 2), Width - 2, this, destDirectoryPath, OnCopyEvent);
            ClickableControls.Add(textBox);

            ClickableControls.Add(new Button(new Position(Width / 2 - 9, Height - 2), 4, this, "OK", OnCopyEvent));
            ClickableControls.Add(new Button(new Position(Width / 2 + 3, Height - 2), 8, this, "Cancel", OnCancelEvent));

            Controls.Add(new Separator(new Position(0, Height - 3), new Size(Width, 1), this));
        }

        private void OnCancelEvent(object sender, EventArgs e)
        {
            Program.Area = Program.MainArea;
            Program.BackgroundArea = null;
        }

        private void OnCopyEvent(object sender, EventArgs e)
        {
            var model = new Model.Model();

            try
            {
                switch (title)
                {
                    case "Copy":
                        model.CopySourceTo(srcDirectoryPath, textBox.Text);

                        Program.Area = Program.MainArea;
                        Program.BackgroundArea = null;
                        break;
                    case "Replace":
                    case "Rename":
                        model.ReplaceSourceTo(srcDirectoryPath, textBox.Text);

                        Program.Area = Program.MainArea;
                        Program.BackgroundArea = null;
                        break;
                }

                Program.Area = Program.MainArea;
                Program.BackgroundArea = null;
            }
            catch (Exception ex)
            {
                int width = ex.Message.Length + 4;
                var size = new Size(width, 5);
                var dialog = new ErrorDialog(size, OnCancelEvent, ex);

                Program.Area = dialog;
                Program.BackgroundArea = this;
            }
        }
    }
}
