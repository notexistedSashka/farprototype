﻿using FarPrototype.View.Dialogs.Controls;
using System;
using System.Collections.Generic;
using System.Text;

namespace FarPrototype.View.Dialogs
{
    public class DeleteDialog : Dialog
    {
        private string fileName = string.Empty;

        public DeleteDialog(Size size, string fileName) : base(size, new FrontBackColor(ConsoleColor.White, ConsoleColor.DarkRed), "Delete")
        {
            this.fileName = fileName;

            Build();
        }

        protected override void Build()
        {
            Controls.Add(new Label(new Position(1, 1), new Size(Width - 2, 1), this, "Do you want to delete file", true));
            Controls.Add(new Label(new Position(1, 2), new Size(Width - 2, 1), this, fileName, true));

            ClickableControls.Add(new Button(new Position(Width / 2 - 9, Height - 2), 5, this, "Yes", OnDelecteEvent));
            ClickableControls.Add(new Button(new Position(Width / 2 + 1, Height - 2), 8, this, "Cancel", OnCancelEvent));

            Controls.Add(new Separator(new Position(0, Height - 3), new Size(Width, 1), this));
        }

        private void OnCancelEvent(object sender, EventArgs e)
        {
            Program.Area = Program.MainArea;
            Program.BackgroundArea = null;
        }

        private void OnDelecteEvent(object sender, EventArgs e)
        {
            var model = new Model.Model();

            try
            {
                model.DeleteItem(fileName);

                Program.Area = Program.MainArea;
                Program.BackgroundArea = null;
            }
            catch (Exception ex)
            {
                int width = ex.Message.Length + 4;
                var size = new Size(width, 5);
                var dialog = new ErrorDialog(size, OnCancelEvent, ex);

                Program.Area = dialog;
                Program.BackgroundArea = this;
            }
        }
    }
}
