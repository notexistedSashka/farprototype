﻿using FarPrototype.View.Dialogs.Controls;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace FarPrototype.View.Dialogs
{
    class CreateDirectoryDialog : Dialog
    {
        private string directoryPath = string.Empty;

        private TextBox textBox;

        public CreateDirectoryDialog(string directoryPath)
            : base(new Size(35, 6), new FrontBackColor(ConsoleColor.Gray, ConsoleColor.DarkBlue), "Create folder")
        {
            this.directoryPath = directoryPath;

            Build();
        }

        protected override void Build()
        {
            Controls.Add(new Label(new Position(1, 1), new Size(6, 1), this, "Name: "));

            textBox = new TextBox(new Position(1, 2), Width - 2, this, string.Empty, OnCreateEvent);
            ClickableControls.Add(textBox);

            ClickableControls.Add(new Button(new Position(Width / 2 - 9, Height - 2), 4, this, "OK", OnCreateEvent));
            ClickableControls.Add(new Button(new Position(Width / 2 + 3, Height - 2), 8, this, "Cancel", OnCancelEvent));

            Controls.Add(new Separator(new Position(0, Height - 3), new Size(Width, 1), this));
        }

        private void OnCancelEvent(object sender, EventArgs e)
        {
            Program.Area = Program.MainArea;
            Program.BackgroundArea = null;
        }

        private void OnCreateEvent(object sender, EventArgs e)
        {
            var model = new Model.Model();

            try
            {
                model.CreateDirectory(Path.Combine(directoryPath, textBox.Text));

                Program.Area = Program.MainArea;
                Program.BackgroundArea = null;
            }
            catch (Exception ex)
            {
                int width = ex.Message.Length + 4;
                var size = new Size(width, 5);
                var dialog = new ErrorDialog(size, OnCancelEvent, ex);

                Program.Area = dialog;
                Program.BackgroundArea = this;
            }
        }
    }
}
