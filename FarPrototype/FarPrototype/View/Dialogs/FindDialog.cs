﻿using FarPrototype.View.Dialogs.Controls;
using FarPrototype.View.Windows;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace FarPrototype.View.Dialogs
{
    class FindDialog : Dialog
    {
        private string directoryPath = string.Empty;

        private TextBox textBox;
        private CheckBox checkBox;

        public FindDialog(string directoryPath)
            : base(new Size(35, 8), new FrontBackColor(ConsoleColor.Gray, ConsoleColor.DarkBlue), "Find")
        {
            this.directoryPath = directoryPath;

            Build();
        }

        protected override void Build()
        {
            Controls.Add(new Label(new Position(1, 1), new Size(9, 1), this, "Pattern: "));

            textBox = new TextBox(new Position(1, 2), Width - 2, this, "*.*", OnFindEvent);
            ClickableControls.Add(textBox);

            checkBox = new CheckBox(new Position(1, Height - 4), Width - 2, this, "find in subfolders", true);
            ClickableControls.Add(checkBox);

            ClickableControls.Add(new Button(new Position(Width / 2 - 9, Height - 2), 4, this, "OK", OnFindEvent));
            ClickableControls.Add(new Button(new Position(Width / 2 + 3, Height - 2), 8, this, "Cancel", OnCancelEvent));

            Controls.Add(new Separator(new Position(0, Height - 3), new Size(Width, 1), this));
            Controls.Add(new Separator(new Position(0, Height - 5), new Size(Width, 1), this));
        }

        private void OnCancelEvent(object sender, EventArgs e)
        {
            Program.Area = Program.MainArea;
            Program.BackgroundArea = null;
        }

        private void OnFindEvent(object sender, EventArgs e)
        {
            var model = new Model.Model();

            try
            {
                Program.ViewWithHotkeys = new UserMenu(model.FindFiles(directoryPath, textBox.Text, checkBox.IsChecked));
            }
            catch (Exception ex)
            {
                int width = ex.Message.Length + 4;
                var size = new Size(width, 5);
                var dialog = new ErrorDialog(size, OnCancelEvent, ex);

                Program.Area = dialog;
                Program.BackgroundArea = this;
            }
        }
    }
}
