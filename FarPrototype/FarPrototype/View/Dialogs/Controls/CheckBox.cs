﻿using FarPrototype.Connector;
using System;
using System.Collections.Generic;
using System.Text;

namespace FarPrototype.View.Dialogs.Controls
{
    class CheckBox : IClickableControl
    {
        public Dictionary<string, IConnector> Commands { get; set; }
        public Dictionary<string, IParameterizedConnector> ParameterizedCommands { get; set; }

        private bool isChecked;
        public bool IsChecked { get => isChecked; set { isChecked = value; Show(); } }

        private string text;

        public CheckBox(Position position, int width, Dialog parent, string text, bool isChecked)
            : base(position, new Size(width, 1), parent, null)
        {
            this.text = text;
            IsChecked = isChecked;

            AddClickEvent(OnEnterEvent);
        }

        public override void Show()
        {
            // set colors
            base.Show();

            Console.SetCursorPosition(PositionX, PositionY);

            char ch = IsChecked ? 'x' : ' ';
            Console.Write($"[{ch}] {text}");

            Console.SetCursorPosition(PositionX + 1, PositionY);
        }

        private void OnEnterEvent(object sender, EventArgs e) => IsChecked = !IsChecked;
    }
}
