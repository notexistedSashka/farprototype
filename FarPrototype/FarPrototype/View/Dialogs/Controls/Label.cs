﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FarPrototype.View.Dialogs.Controls
{
    class Label : IDialogControl
    {
        private string text;
        private bool alignCenter;

        public Label(Position position, Size size, Dialog parent, string text, bool alignCenter = false)
            : base(position, size, parent)
        {
            this.text = text;
            this.alignCenter = alignCenter;
        }

        public override void Show()
        {
            Console.ForegroundColor = Colors.Front;
            Console.BackgroundColor = Colors.Back;

            if (Width < text.Length)
                text = text.Substring(0, Width);

            if (alignCenter)
                Console.SetCursorPosition(PositionX + (Width - text.Length) / 2, PositionY);
            else
                Console.SetCursorPosition(PositionX, PositionY);

            Console.Write(text);
        }
    }
}
