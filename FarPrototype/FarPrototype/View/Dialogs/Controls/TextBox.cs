﻿using FarPrototype.Common;
using FarPrototype.Connector;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace FarPrototype.View.Dialogs.Controls
{
    // TODO: Finish
    class TextBox : IClickableControl, IHasHotkeys
    {
        private string text;
        public string Text
        {
            get => text;
            set
            {
                text = value;
                Show();
            }
        }

        private int cursorPosition;
        public int CursorPosition
        {
            get => cursorPosition;
            set
            {
                cursorPosition = value;
                PrintText();
            }
        }

        private int printFrom;

        public Dictionary<string, IConnector> Commands { get; set; }
        public Dictionary<string, IParameterizedConnector> ParameterizedCommands { get; set; }

        public TextBox(Position position, int width, Dialog parent, string text, EventHandler handler)
            : base(position, new Size(width, 1), parent, handler)
        {
            Text = text;
            CursorPosition = text.Length;
            printFrom = 0;

            CreateCommands();
        }

        private void CreateCommands()
        {
            Commands = new Dictionary<string, IConnector>();

            Commands.Add(ConsoleKey.Tab.ToString() + "FalseFalseFalse", new TextBoxUnselectToNextCommand(this));
            Commands.Add(ConsoleKey.Tab.ToString() + "FalseFalseTrue", new TextBoxUnselectToPreviousCommand(this));
            Commands.Add(ConsoleKey.Enter.ToString() + "FalseFalseFalse", new TextBoxEnterClickCommand(this));
            Commands.Add(ConsoleKey.Escape.ToString() + "FalseFalseFalse", new TextBoxEscapeClickCommand(this));

            // IHasHotkeys
            Commands.Add(ConsoleKey.Backspace.ToString() + "FalseFalseFalse", new TypeBackspaceCommand(this));
            Commands.Add(ConsoleKey.Backspace.ToString() + "FalseTrueFalse", new TypeBackspaceCtrlCommand(this));
            Commands.Add(ConsoleKey.Delete.ToString() + "FalseFalseFalse", new TypeDeleteCommand(this));
            Commands.Add(ConsoleKey.Delete.ToString() + "FalseTrueFalse", new TypeDeleteCtrlCommand(this));

            Commands.Add(ConsoleKey.Home.ToString() + "FalseFalseFalse", new TypeHomeCommand(this));
            Commands.Add(ConsoleKey.End.ToString() + "FalseFalseFalse", new TypeEndCommand(this));
            Commands.Add(ConsoleKey.UpArrow.ToString() + "FalseFalseFalse", new TypeUpArrowCommand(this));
            Commands.Add(ConsoleKey.DownArrow.ToString() + "FalseFalseFalse", new TypeDownArrowCommand(this));
            Commands.Add(ConsoleKey.RightArrow.ToString() + "FalseFalseFalse", new TypeRightArrowCommand(this));
            Commands.Add(ConsoleKey.LeftArrow.ToString() + "FalseFalseFalse", new TypeLeftArrowCommand(this));

            ParameterizedCommands = new Dictionary<string, IParameterizedConnector>();

            for (var i = 0; i < 2048; i++) // Set commands for most chars with shift and without
            {
                char ch = Convert.ToChar(i);

                if (char.IsLetterOrDigit(ch) || char.IsPunctuation(ch) || char.IsWhiteSpace(ch))
                {
                    ParameterizedCommands.Add(ch + "FalseFalseFalse", new TypingSymbolPressedCommand(this));
                    ParameterizedCommands.Add(ch + "FalseFalseTrue", new TypingSymbolPressedCommand(this));
                }
            }
        }

        public override void Show()
        {
            Console.BackgroundColor = ConsoleColor.DarkCyan;

            Console.SetCursorPosition(PositionX, PositionY);
            for (int i = 0; i < Width; i++)
                Console.Write(' ');

            PrintText();
        }

        private void PrintText()
        {
            Console.ForegroundColor = ConsoleColor.Black;
            Console.BackgroundColor = ConsoleColor.DarkCyan;

            string shownText = Text;

            printFrom = 0;
            if (Width - 1 < Text.Length)
            {
                printFrom = Text.Length - Width + 1;

                if (CursorPosition < printFrom)
                    printFrom = CursorPosition;

                shownText = Text.Substring(printFrom, Width - 1);
            }

            Console.SetCursorPosition(PositionX, PositionY);
            Console.Write(shownText);
            Console.SetCursorPosition(PositionX + CursorPosition - printFrom, PositionY);
        }

        public override void Select()
        {
            Program.ViewWithHotkeys = this;

            Console.CursorVisible = true;
        }

        public override void Unselect()
        {
            Program.ViewWithHotkeys = null;

            Console.CursorVisible = false;
        }

        public void RedirectToParent(string command)
        {
            parent.Commands[command].Execute();
        }

        public void BackSpaceClick(bool withCtrl)
        {
            if (withCtrl)
            {
                if (CursorPosition < Text.Length)
                    Text = Text.Substring(CursorPosition, Text.Length - CursorPosition);
                else
                    Text = string.Empty;
                CursorPosition = 0;
                Show();
            }
            else
            {
                if (Text != string.Empty && CursorPosition > 0)
                {
                    CursorPosition--;
                    Text = Text.Remove(CursorPosition, 1);
                }
            }
        }

        public void EndClick()
        {
            CursorPosition = Text.Length;
        }

        public void HomeClick()
        {
            CursorPosition = 0;
        }

        public void DeleteClick(bool withCtrl)
        {
            if (withCtrl)
            {
                if (CursorPosition > 0)
                    Text = Text.Substring(0, CursorPosition);
                else
                    Text = string.Empty;
                Show();
            }
            else
            {
                if (Text != string.Empty && CursorPosition < Text.Length)
                    Text = Text.Remove(CursorPosition, 1);
            }
        }

        public void ArrowClick(Arrow arrow, bool withCtrl)
        {
            switch (arrow)
            {
                case Arrow.Left:
                    if (CursorPosition > 0)
                        CursorPosition--;
                    break;
                case Arrow.Right:
                    if (CursorPosition < Text.Length)
                        CursorPosition++;
                    break;
            }
        }

        public void TypeSymbol(string s)
        {
            Text = Text.Insert(CursorPosition, s);
            CursorPosition++;
        }
    }
}
