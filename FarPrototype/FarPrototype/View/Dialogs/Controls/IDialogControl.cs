﻿using FarPrototype.View.Windows;
using System;
using System.Collections.Generic;
using System.Text;

namespace FarPrototype.View.Dialogs.Controls
{
    public abstract class IDialogControl : IWindow
    {
        public FrontBackColor Colors { get; set; }

        protected Dialog parent;

        public override int PositionX { get => positionX + parent.PositionX; set => positionX = value; }
        public override int PositionY { get => positionY + parent.PositionY; set => positionY = value; }

        public IDialogControl(Position position, Size size, Dialog parent)
            : base(position, size)
        {
            this.parent = parent;
            Colors = parent.Colors;
        }
    }
}
