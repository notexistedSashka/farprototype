﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FarPrototype.View.Dialogs.Controls
{
    class Button : IClickableControl
    {
        private string text;

        public Button(Position position, int width, Dialog parent, string text, EventHandler eventHandler)
            : base(position, new Size(width, 1), parent, eventHandler)
        {
            this.text = text;
        }

        public override void Show()
        {
            // Sets colors
            base.Show();

            if (Width < text.Length)
            {
                text = text.Substring(0, Width);
                Console.SetCursorPosition(PositionX, PositionY);
            }
            else if (Width > text.Length)
            {
                Console.SetCursorPosition(PositionX + (Width - text.Length) / 2, PositionY);
            }

            Console.SetCursorPosition(PositionX, PositionY);
            Console.Write($"[ {text} ]");
        }
    }
}
