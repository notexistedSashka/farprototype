﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FarPrototype.View.Dialogs.Controls
{
    public abstract class IClickableControl : IDialogControl
    {
        private event EventHandler clickEvent;
        public bool IsSelected { get; set; }

        public IClickableControl(Position position, Size size, Dialog parent, EventHandler eventHandler)
            : base(position, size, parent)
        {
            IsSelected = false;
            clickEvent += eventHandler;
        }

        protected void AddClickEvent(EventHandler eventHandler)
        {
            clickEvent += eventHandler;
        }

        public virtual void Select()
        {
            IsSelected = true;
            Show();
        }

        public virtual void Unselect()
        {
            IsSelected = false;
            Show();
        }

        public void Execute() // Enter click
        {
            if(clickEvent != null)
                clickEvent.Invoke(this, EventArgs.Empty);
        }

        public override void Show()
        {
            Console.ForegroundColor = IsSelected ? Colors.Back : Colors.Front;
            Console.BackgroundColor = IsSelected ? Colors.Front : Colors.Back;
        }
    }
}
