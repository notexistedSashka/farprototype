﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FarPrototype.View.Dialogs.Controls
{
    class Separator : IDialogControl
    {
        private string separator;

        public Separator(Position position, Size size, Dialog parent)
            : base(position, size, parent)
        {
            separator = string.Empty;

            separator += '╟';

            for (int i = 0; i < Width - 2; i++)
                separator += '─';

            separator += '╢';
        }

        public override void Show()
        {
            Console.ForegroundColor = Colors.Front;
            Console.BackgroundColor = Colors.Back;

            Console.SetCursorPosition(PositionX, PositionY);
            Console.Write(separator);
        }
    }
}
