﻿using FarPrototype.Common;
using FarPrototype.Connector;
using FarPrototype.View.Areas;
using FarPrototype.View.Dialogs.Controls;
using FarPrototype.View.Windows;
using System;
using System.Collections.Generic;
using System.Text;

namespace FarPrototype.View.Dialogs
{
    public abstract class Dialog : IWindow, IArea
    {
        public Dictionary<string, IConnector> Commands { get; set; }
        public Dictionary<string, IParameterizedConnector> ParameterizedCommands { get; set; }

        public FrontBackColor Colors { get; set; }

        public List<IDialogControl> Controls { get; set; }
        public List<IClickableControl> ClickableControls { get; set; }

        private int currentControlIndex = 0;

        protected char[,] display;
        private string title;

        public Dialog(Size size, FrontBackColor colors, string title) :
            base (new Position((Console.WindowWidth - size.Width) / 2, (Console.WindowHeight - size.Height) / 2), size)
        {
            Colors = colors;
            this.title = title;

            CreateDisplay();
            CreateCommands();
            CreateWindows();
        }

        protected abstract void Build();

        protected void CreateDisplay()
        {
            display = new char[Height, Width];

            for (int y = 0; y < Height; y++)
            {
                for (int x = 0; x < Width; x++)
                {
                    if (x == 0 && y == 0)
                        display[y, x] = '╔';
                    else if (x == Width - 1 && y == 0)
                        display[y, x] = '╗';
                    else if (x == 0 && y == Height - 1)
                        display[y, x] = '╚';
                    else if (x == Width - 1 && y == Height - 1)
                        display[y, x] = '╝';
                    else if (x == 0 || x == Width - 1)
                        display[y, x] = '║';
                    else if (y == 0 || y == Height - 1)
                        display[y, x] = '═';
                    else
                        display[y, x] = ' ';
                }
            }
        }

        protected void CreateCommands()
        {
            Commands = new Dictionary<string, IConnector>();

            Commands.Add(ConsoleKey.Tab.ToString() + "FalseFalseFalse", new SelectNextControlCommand(this));
            Commands.Add(ConsoleKey.Tab.ToString() + "FalseFalseTrue", new SelectPreviousControlCommand(this));
            Commands.Add(ConsoleKey.Enter.ToString() + "FalseFalseFalse", new ExecuteControlCommand(this));
            Commands.Add(ConsoleKey.Escape.ToString() + "FalseFalseFalse", new ExecuteCancelCommand(this));

            ParameterizedCommands = new Dictionary<string, IParameterizedConnector>();
        }

        public void CreateWindows()
        {
            Controls = new List<IDialogControl>();
            ClickableControls = new List<IClickableControl>();
        }

        public void ResizeWindows()
        {
            SetPosition((Console.WindowWidth - Width) / 2, (Console.WindowHeight - Height) / 2);

        }

        public void SelectNextControl()
        {
            if (currentControlIndex < ClickableControls.Count)
            {
                ClickableControls[currentControlIndex].Unselect();

                currentControlIndex = currentControlIndex + 1 < ClickableControls.Count ? currentControlIndex + 1 : 0;

                ClickableControls[currentControlIndex].Select();
            }
        }

        public void SelectPreviousControl()
        {
            if (currentControlIndex < ClickableControls.Count)
            {
                ClickableControls[currentControlIndex].Unselect();

                currentControlIndex = currentControlIndex - 1 >= 0 ? currentControlIndex - 1 : ClickableControls.Count - 1;

                ClickableControls[currentControlIndex].Select();
            }
        }

        public void ExecuteControl()
        {
            if (currentControlIndex < ClickableControls.Count)
                ClickableControls[currentControlIndex].Execute();
        }

        public override void Show()
        {
            Console.CursorVisible = false;

            var lines = new List<string>();
            string tmpLine;

            for (int y = 0; y < Height; y++)
            {
                tmpLine = string.Empty;

                for (int x = 0; x < Width; x++)
                    tmpLine += display[y, x];

                lines.Add(tmpLine);
            }

            Console.ForegroundColor = Colors.Front;
            Console.BackgroundColor = Colors.Back;
            for (int i = 0; i < Height; i++)
            {
                Console.SetCursorPosition(PositionX, PositionY + i);
                Console.Write(lines[i]);
            }

            if (title.Length <= Width)
            {
                Console.SetCursorPosition(PositionX + Width / 2 - title.Length / 2, PositionY);
                Console.Write(title);
            }
            else
            {
                Console.SetCursorPosition(PositionX + 1, PositionY);
                Console.Write($"{title.Substring(0, Width - 5)}...");
            }

            foreach (var window in Controls)
                window.Show();

            foreach (var window in ClickableControls)
                window.Show();

            if(ClickableControls.Count != 0)
                ClickableControls[currentControlIndex].Select();
        }

        public override void SetSize(int width, int posY)
        {
            Width = width;
            Height = Height;

            CreateDisplay();
        }

        public void BackSpaceClick(bool withCtrl)
        {
            throw new NotImplementedException();
        }

        public void EndClick()
        {
            throw new NotImplementedException();
        }

        public void HomeClick()
        {
            throw new NotImplementedException();
        }

        public void DeleteClick(bool withCtrl)
        {
            throw new NotImplementedException();
        }

        public void ArrowClick(Arrow arrow, bool withCtrl)
        {
            throw new NotImplementedException();
        }

        public void TypeSymbol(string s)
        {
            throw new NotImplementedException();
        }
    }
}
