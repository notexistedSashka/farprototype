﻿using FarPrototype.View.Dialogs.Controls;
using System;
using System.Collections.Generic;
using System.Text;

namespace FarPrototype.View.Dialogs
{
    class ErrorDialog : Dialog
    {
        private string error = string.Empty;
        private EventHandler handler;

        public ErrorDialog(Size size, EventHandler handler, string errorMsg) : base(size, new FrontBackColor(ConsoleColor.White, ConsoleColor.DarkRed), "Error")
        {
            this.handler = handler;
            error = errorMsg;
            Build();
        }

        public ErrorDialog(Size size, EventHandler handler, Exception exception) : base(size, new FrontBackColor(ConsoleColor.White, ConsoleColor.DarkRed), "Error")
        {
            this.handler = handler;
            error = exception.Message;
            Build();
        }

        protected override void Build()
        {
            Controls.Add(new Label(new Position(1, 1), new Size(Width - 2, 1), this, error, true));

            ClickableControls.Add(new Button(new Position(Width / 2 - 2, Height - 2), 4, this, "OK", handler));

            Controls.Add(new Separator(new Position(0, Height - 3), new Size(Width, 1), this));
        }
    }
}
