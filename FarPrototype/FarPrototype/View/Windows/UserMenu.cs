﻿using FarPrototype.Common;
using FarPrototype.Connector;
using FarPrototype.View.Areas;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace FarPrototype.View.Windows
{
    // TODO: try catch
    public class UserMenu : IWindow, IHasHotkeys
    {
        private List<string> items;

        public string CurrentPath { get => items[currentItemIndex].Trim(); }

        public Dictionary<string, IConnector> Commands { get; set; }
        public Dictionary<string, IParameterizedConnector> ParameterizedCommands { get; set; }

        private char[,] display;
        private int currentItemIndex = 0;
        private int showItemsFrom = 0;

        public UserMenu(List<string> items = null) : base(new Position(5, 4), new Size(Console.WindowWidth - 10, Console.WindowHeight - 8))
        {
            this.items = items;

            if (items == null)
            {
                GetItems();
            }
            else
                this.items = items;

            CreateDisplay();
            AlignItems();

            CreateCommands();
        }

        private void CreateCommands()
        {
            Commands = new Dictionary<string, IConnector>();

            // Change cursor position
            Commands.Add(ConsoleKey.UpArrow.ToString() + "FalseFalseFalse", new UserMenuMoveCursorUpCommand(this));
            Commands.Add(ConsoleKey.DownArrow.ToString() + "FalseFalseFalse", new UserMenuMoveCursorDownCommand(this));
            Commands.Add(ConsoleKey.RightArrow.ToString() + "FalseFalseFalse", new UserMenuMoveCursorRightCommand(this));
            Commands.Add(ConsoleKey.LeftArrow.ToString() + "FalseFalseFalse", new UserMenuMoveCursorLeftCommand(this));
            Commands.Add(ConsoleKey.Home.ToString() + "FalseFalseFalse", new UserMenuMoveCursorHomeCommand(this));
            Commands.Add(ConsoleKey.End.ToString() + "FalseFalseFalse", new UserMenuMoveCursorEndCommand(this));

            // Close
            Commands.Add(ConsoleKey.Escape.ToString() + "FalseFalseFalse", new UserMenuCloseCommand(this));
            Commands.Add(ConsoleKey.F10.ToString() + "FalseFalseFalse", new UserMenuCloseCommand(this));

            // Execute
            Commands.Add(ConsoleKey.Enter.ToString() + "FalseFalseFalse", new UserMenuExecuteItemCommand(this));

            ParameterizedCommands = new Dictionary<string, IParameterizedConnector>();
        }

        private void CreateDisplay()
        {
            display = new char[Height, Width];

            for (int y = 0; y < Height; y++)
            {
                for (int x = 0; x < Width; x++)
                {
                    if (x == 0 && y == 0)
                        display[y, x] = '╔';
                    else if (x == Width - 1 && y == 0)
                        display[y, x] = '╗';
                    else if (x == 0 && y == Height - 1)
                        display[y, x] = '╚';
                    else if (x == Width - 1 && y == Height - 1)
                        display[y, x] = '╝';
                    else if (x == 0 || x == Width - 1)
                        display[y, x] = '║';
                    else if (y == 0 || y == Height - 1)
                        display[y, x] = '═';
                    else
                        display[y, x] = ' ';
                }
            }
        }

        public override void Show()
        {
            Console.CursorVisible = false;

            Console.ForegroundColor = ConsoleColor.Gray;
            Console.BackgroundColor = ConsoleColor.DarkBlue;

            var lines = new List<string>();

            string line;
            for (int y = 0; y < Height; y++)
            {
                line = string.Empty;
                for (int x = 0; x < Width; x++)
                    line += display[y, x];
                lines.Add(line);
            }

            for (int y = 0; y < Height; y++)
            {
                Console.SetCursorPosition(PositionX, y + PositionY);
                Console.Write(lines[y]);
            }

            string title = "User menu";
            Console.SetCursorPosition(PositionX + Width / 2 - title.Length / 2, PositionY);
            Console.Write(title);

            ShowItems();
        }

        private void AlignItems()
        {
            for (int i = 0; i < items.Count; i++)
            {
                if (items[i].Length < Width - 2)
                    for (int j = items[i].Length; j < Width - 2; j++)
                        items[i] += " ";
            }
        }

        private void ShowItems()
        {
            Console.ForegroundColor = ConsoleColor.Gray;

            for (int i = 0; i < Height - 2; i++)
            {
                if (i + showItemsFrom < items.Count)
                {
                    string item = items[i + showItemsFrom];

                    Console.SetCursorPosition(PositionX + 1, PositionY + 1 + i);

                    if (i + showItemsFrom == currentItemIndex)
                        Console.BackgroundColor = ConsoleColor.Black;
                    else
                        Console.BackgroundColor = ConsoleColor.DarkBlue;

                    if (item.Length > Width - 1)
                        item = "{" + item.Substring(item.Length - Width + 3, Width - 3);

                    Console.Write(item);
                }
            }
        }

        private void GetItems()
        {
            items = new List<string>();

            items.Add(@"C:\");
            items.Add(@"D:\");
            items.Add(@"C:\Users\User\Downloads");
            items.Add(@"D:\Programming\Projects");
        }

        public void ChangeActiveItem(ConsoleKey key)
        {
            switch (key)
            {
                case ConsoleKey.DownArrow:
                    if (currentItemIndex < items.Count - 1)
                    {
                        currentItemIndex++;
                        if (currentItemIndex > showItemsFrom + Height - 3)
                            showItemsFrom++;
                    }
                    break;
                case ConsoleKey.UpArrow:
                    if (currentItemIndex - 1 >= 0)
                    {
                        currentItemIndex--;
                        if (currentItemIndex == showItemsFrom - 1 && showItemsFrom > 0)
                            showItemsFrom--;
                    }
                    break;
                case ConsoleKey.Home:
                    currentItemIndex = 0;
                    showItemsFrom = 0;
                    break;
                case ConsoleKey.End:
                    currentItemIndex = items.Count - 1;
                    if (currentItemIndex > showItemsFrom + Height - 3)
                        showItemsFrom = showItemsFrom = currentItemIndex - Height + 3;
                    break;
            }

            ShowItems();
        }

        public void ExecuteItem()
        {
            if (!Directory.Exists(CurrentPath))
            {
                var proc = new Process();
                proc.StartInfo.FileName = CurrentPath;
                proc.StartInfo.UseShellExecute = true;
                proc.Start();
            }
            else
            {
                Program.MainArea.SetDirectoryPath(CurrentPath);
                Program.ViewWithHotkeys = null;
            }
        }

        public void BackSpaceClick(bool withCtrl)
        {
            throw new NotImplementedException();
        }

        public void EndClick()
        {
            throw new NotImplementedException();
        }

        public void HomeClick()
        {
            throw new NotImplementedException();
        }

        public void DeleteClick(bool withCtrl)
        {
            throw new NotImplementedException();
        }

        public void ArrowClick(Arrow arrow, bool withCtrl)
        {
            throw new NotImplementedException();
        }

        public void TypeSymbol(string s)
        {
            throw new NotImplementedException();
        }
    }
}
