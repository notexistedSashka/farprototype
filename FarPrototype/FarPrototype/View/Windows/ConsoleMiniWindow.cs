﻿using FarPrototype.Common;
using FarPrototype.Model;
using FarPrototype.View.Dialogs;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace FarPrototype.View.Windows
{
    class ConsoleMiniWindow : IWindow
    {
        private string directoryPath;
        public string DirectoryPath
        {
            get
            {
                return directoryPath;
            }
            set
            {
                directoryPath = value;
                Show();
            }
        }

        private string cmdCommand;
        public string CmdCommand
        {
            get
            {
                return cmdCommand;
            }
            set
            {
                cmdCommand = value;
                Show();
            }
        }

        private int cursorPosition;
        public int CursorPosition
        {
            get
            {
                return cursorPosition;
            }
            set
            {
                cursorPosition = value;
                Show();
            }
        }

        public ConsoleMiniWindow(Position position, Size size, string directoryPath) : base(position, size)
        {
            this.directoryPath = directoryPath;
            cmdCommand = string.Empty;
            CursorPosition = 0;
        }

        public override void Show()
        {
            // TODO: make resizeable print
            Console.SetCursorPosition(PositionX, PositionY);

            Console.ForegroundColor = ConsoleColor.White;
            Console.BackgroundColor = ConsoleColor.Black;

            for (int i = 0; i < Width; i++)
                Console.Write(' ');

            Console.SetCursorPosition(PositionX, PositionY);
            Console.Write($"{directoryPath}> {CmdCommand}");

            CommonData.cursorCoords = new Tuple<int, int>(directoryPath.Length + 2 + CursorPosition, PositionY);
            Console.SetCursorPosition(PositionX + CursorPosition + directoryPath.Length + 2, PositionY);
        }

        public void ExecutingUserCommand(CmdWindow cmdWindow) => ExecutingUserCommand(cmdWindow, CmdCommand);

        public void ExecutingUserCommand(CmdWindow cmdWindow, string command)
        {
            CmdCommand = string.Empty;
            CursorPosition = 0;

            command = command.Trim();

            cmdWindow.Answers.Add($"{directoryPath}> {command}");

            if (command.Equals("exit"))
            {
                Console.ResetColor();
                Environment.Exit(1);
            }
            else if (command.StartsWith("cd "))
            {
                string path = command.Substring(3, command.Length - 3).Trim();
                if (Directory.Exists(Path.Combine(directoryPath, path)))
                {
                    directoryPath = Path.GetFullPath(Path.Combine(directoryPath, path));
                }
                else if (Directory.Exists(path))
                {
                    directoryPath = path;
                }
                else
                {
                    string msg = "Directory does not exist";
                    int width = msg.Length + 4;
                    var size = new Size(width, 5);
                    var dialog = new ErrorDialog(size, OnCancelEvent, msg);

                    Program.Area = dialog;
                    Program.BackgroundArea = Program.CmdArea;
                }
                cmdWindow.Answers.Add(string.Empty);
                cmdWindow.Show();
                Show();
                return;
            }

            var stream = Cmd.GetCommandResult(directoryPath, command);

            Console.SetCursorPosition(CommonData.cursorCoords.Item1, CommonData.cursorCoords.Item2);

            while (!stream.EndOfStream)
            {
                string line = stream.ReadLine();

                cmdWindow.Answers.Add(line);
                cmdWindow.Show();
                Show();
            }
            cmdWindow.Answers.Add(string.Empty);
            cmdWindow.Show();
            Show();
        }

        // Events
        // ------
        private void OnCancelEvent(object sender, EventArgs e)
        {
            Program.Area = Program.CmdArea;
            Program.BackgroundArea = null;
        }
    }
}
