﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using FarPrototype.Common;
using FarPrototype.Model;

namespace FarPrototype.View.Windows
{
    class PannelWindow : IWindow
    {
        private object locker = new object();
        private string directoryPath;
        public string DirectoryPath
        {
            get
            {
                return directoryPath;
            }
            set
            {
                lock (locker)
                {
                    directoryPath = value;
                    currentItemIndex = 0;
                    showItemsFrom = 0;
                    GetSubItems();
                    AlignItems();
                }
            }
        }

        public string CurrentItem => subItems[currentItemIndex].Trim();
        private List<string> subItems;
        public bool IsActive { get; set; } = false;
        public bool IsHidden { get; set; } = false;

        private char[,] display;
        private int currentItemIndex = 0;
        private int showItemsFrom = 0;

        public PannelWindow(Position position, Size size, string directoryPath) : base(position, size)
        {
            this.directoryPath = directoryPath;

            CreateDisplay();
            GetSubItems();
            AlignItems();

            var thread = new Thread(new ThreadStart(CheckNewItemsThread));
            thread.Start();
        }

        private void CreateDisplay()
        {
            display = new char[Height, Width];

            for (int y = 0; y < Height; y++)
            {
                for (int x = 0; x < Width; x++)
                {
                    if (x == 0 && y == 0)
                        display[y, x] = '╔';
                    else if (x == Width - 1 && y == 0)
                        display[y, x] = '╗';
                    else if (x == 0 && y == Height - 1)
                        display[y, x] = '╚';
                    else if (x == Width - 1 && y == Height - 1)
                        display[y, x] = '╝';
                    else if (x == Width / 2 && y < Height - 3)
                        display[y, x] = '║';
                    else if (x == Width / 2 && y == Height - 3)
                        display[y, x] = '╨';
                    else if (y == Height - 3 && x == 0)
                        display[y, x] = '╟';
                    else if (y == Height - 3 && x == Width - 1)
                        display[y, x] = '╢';
                    else if (y == Height - 3)
                        display[y, x] = '─';
                    else if (x == 0 || x == Width - 1)
                        display[y, x] = '║';
                    else if (y == 0 || y == Height - 1)
                        display[y, x] = '═';
                    else
                        display[y, x] = ' ';
                }
            }
        }

        public override void SetSize(int width, int height)
        {
            Width = width;
            Height = height;

            CreateDisplay();
            GetSubItems();
            AlignItems();
        }

        public override void SetPosition(int posX, int posY)
        {
            PositionX = posX;
            PositionY = posY;

            CreateDisplay();
        }

        public override void Show()
        {
            lock (locker)
            {
                if (!IsHidden)
                {
                    Console.ForegroundColor = ConsoleColor.Cyan;
                    Console.BackgroundColor = ConsoleColor.Black;

                    var lines = new List<string>();

                    string line;
                    for (int y = 0; y < Height; y++)
                    {
                        line = string.Empty;
                        for (int x = 0; x < Width; x++)
                            line += display[y, x];
                        lines.Add(line);
                    }

                    for (int y = 0; y < Height; y++)
                    {
                        Console.SetCursorPosition(PositionX, y + PositionY);
                        Console.Write(lines[y]);
                    }

                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.SetCursorPosition(PositionX + Width / 4, PositionY + 1);
                    Console.Write("Name");
                    Console.SetCursorPosition(PositionX + Width - Width / 4, PositionY + 1);
                    Console.Write("Name");

                    UpdateForNewPathPannel();
                    ShowItems();
                    ShowCurrentItemBottomInfo();

                    Console.SetCursorPosition(CommonData.cursorCoords.Item1, CommonData.cursorCoords.Item2);
                }
            }
        }

        public void UpdateForNewPathPannel()
        {
            // TODO: remake as a human
            Console.BackgroundColor = ConsoleColor.Black;
            // Hide previous items
            string line = "";

            for (int i = 0; i < Width / 2 - 2; i++)
                line += ' ';

            int posx = PositionX + 1;
            for (int i = 0; i < Height - 5; i++)
            {
                Console.SetCursorPosition(posx, PositionY + 2 + i);
                Console.Write(line);

                if (i == Height - 6 && posx == PositionX + 1)
                {
                    i = -1;
                    posx = PositionX + 1 + Width / 2;
                }
            }

            // Hide previous path and bottom info
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.SetCursorPosition(PositionX + 1, PositionY);
            for (int i = 0; i < Width - 2; i++)
                Console.Write('═');
            Console.SetCursorPosition(PositionX + 1, Height - 1);
            for (int i = 0; i < Width - 2; i++)
                Console.Write('═');

            // Set new path
            Console.ForegroundColor = ConsoleColor.Yellow;
            if (DirectoryPath.Length <= Width)
            {
                Console.SetCursorPosition(PositionX + Width / 2 - DirectoryPath.Length / 2, PositionY);
                Console.Write(DirectoryPath);
            }
            else
            {
                Console.SetCursorPosition(PositionX + 1, PositionY);
                Console.Write($"{DirectoryPath.Substring(0, 3)}...{DirectoryPath.Substring(DirectoryPath.Length - Width + 8, Width - 8)}");
            }

            // Set new bottom info
            var model = new Model.Model();
            var info = new DirectoryInfo(DirectoryPath);

            string size = model.GetFilesSizeStringFormat(DirectoryPath);

            string currentDirectoryInfo = $"Bytes: {size}, files: {info.GetFiles().Length}, folders: {info.GetDirectories().Length}";
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.BackgroundColor = ConsoleColor.Black;
            Console.SetCursorPosition(PositionX + Width / 2 - currentDirectoryInfo.Length / 2, PositionY + Height - 1);
            Console.Write(currentDirectoryInfo);
        }

        public void ShowItems()
        {
            Console.CursorVisible = false;

            int xpos = PositionX + 1;
            int ypos = PositionY + 2;
            for (int i = 0; i < subItems.Count; i++)
            {
                string item = subItems[i];

                if (i + showItemsFrom < subItems.Count)
                    item = subItems[i + showItemsFrom];
                string fullName = Path.Combine(DirectoryPath, item);
                if (Directory.Exists(fullName))
                {
                    var info = new DirectoryInfo(fullName);
                    if (info.Attributes.HasFlag(FileAttributes.Hidden))
                        Console.ForegroundColor = ConsoleColor.DarkGray;
                    else
                        Console.ForegroundColor = ConsoleColor.White;
                }
                else
                {
                    var info = new FileInfo(fullName);
                    if (info.Attributes.HasFlag(FileAttributes.Hidden))
                        Console.ForegroundColor = ConsoleColor.DarkGray;
                    else if (info.Extension == ".exe")
                        Console.ForegroundColor = ConsoleColor.Green;
                    else if (CommonData.archiveExtensions.Contains(info.Extension))
                        Console.ForegroundColor = ConsoleColor.Magenta;
                    else
                        Console.ForegroundColor = ConsoleColor.Cyan;
                }

                if (i < Height - 5)
                    Console.SetCursorPosition(xpos, ypos + i);
                else if (i < (Height - 5) * 2)
                    Console.SetCursorPosition(xpos + Width / 2, ypos + i - Height + 5);
                else
                    break;


                if (IsActive && i + showItemsFrom == currentItemIndex)
                    Console.BackgroundColor = ConsoleColor.DarkCyan;
                else
                    Console.BackgroundColor = ConsoleColor.Black;

                if (item.Length > Width / 2 - 1)
                    item = item.Substring(0, Width / 2 - 3) + "}";
                Console.Write(item);
            }

            Console.SetCursorPosition(CommonData.cursorCoords.Item1, CommonData.cursorCoords.Item2);
            Console.CursorVisible = true;
        }

        private void GetSubItems()
        {
            subItems = new List<string>();

            var drives = new List<string>();
            drives.AddRange(Directory.GetLogicalDrives());
            if (!drives.Contains(DirectoryPath))
                subItems.Add("..");

            var model = new Model.Model();
            subItems.AddRange(model.GetItemsFrom(DirectoryPath).ToArray());
        }

        private void AlignItems()
        {
            int columnWidth = (Width - 3) / 2;
            for (int i = 0; i < subItems.Count; i++)
            {
                if (subItems[i].Length < columnWidth)
                    for (int j = subItems[i].Length; j < columnWidth; j++)
                        subItems[i] += " ";
            }
        }

        public void ChangeActiveItem(ConsoleKey button)
        {
            switch (button)
            {
                case ConsoleKey.RightArrow:
                    if (subItems.Count < currentItemIndex + Height - 4)
                        currentItemIndex = subItems.Count - 1;
                    else
                        currentItemIndex += Height - 5;
                    if (currentItemIndex > showItemsFrom + Height * 2 - 11)
                        showItemsFrom = currentItemIndex - Height * 2 + 11;
                    break;
                case ConsoleKey.LeftArrow:
                    if (currentItemIndex > Height - 5 + 10)
                    {
                        currentItemIndex -= Height - 5;
                        if (showItemsFrom > currentItemIndex)
                            showItemsFrom = currentItemIndex;
                    }
                    else
                    {
                        currentItemIndex = 0;
                        showItemsFrom = currentItemIndex;
                    }
                    break;
                case ConsoleKey.DownArrow:
                    if (currentItemIndex < subItems.Count - 1)
                    {
                        currentItemIndex++;
                        if (currentItemIndex > showItemsFrom + Height * 2 - 11)
                            showItemsFrom++;
                    }
                    break;
                case ConsoleKey.UpArrow:
                    if (currentItemIndex - 1 >= 0)
                    {
                        currentItemIndex--;
                        if (currentItemIndex == showItemsFrom - 1 && showItemsFrom > 0)
                            showItemsFrom--;
                    }
                    break;
                case ConsoleKey.Home:
                    currentItemIndex = 0;
                    showItemsFrom = 0;
                    break;
                case ConsoleKey.End:
                    currentItemIndex = subItems.Count - 1;
                    if (currentItemIndex > showItemsFrom + Height * 2 - 11)
                        showItemsFrom = showItemsFrom = currentItemIndex - Height * 2 + 11;
                    break;
            }

            ShowCurrentItemBottomInfo();
            ShowItems();
        }

        private void ShowCurrentItemBottomInfo()
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.BackgroundColor = ConsoleColor.Black;
            Console.SetCursorPosition(PositionX + 1, Height - 2);
            for (int i = 0; i < Width - 2; i++)
                Console.Write(' ');

            var model = new Model.Model();
            var path = Path.Combine(DirectoryPath, CurrentItem);

            string size = string.Empty;

            if (File.Exists(path))
                size = model.GetFilesSizeStringFormat(path);

            string dateTime = model.GetDateTimeItem(path).ToString();

            string rightInfo = string.Concat(size, " ", dateTime);

            Console.SetCursorPosition(PositionX + 1, Height - 2);
            if (CurrentItem.Length + rightInfo.Length < Width)
            {
                Console.Write(CurrentItem);

                Console.SetCursorPosition(PositionX + Width - rightInfo.Length - 1, Height - 2);
                Console.Write(Path.GetFileName(rightInfo));
            }
            else
            {
                Console.Write($"{CurrentItem.Substring(0, Width - rightInfo.Length - 6)}... {rightInfo}");
            }
        }

        private void CheckNewItemsThread()
        {
            while (true)
            {
                var list = new List<string>();

                var drives = new List<string>();
                drives.AddRange(Directory.GetLogicalDrives());
                if (!drives.Contains(DirectoryPath))
                    list.Add("..");

                var model = new Model.Model();
                list.AddRange(model.GetItemsFrom(DirectoryPath).ToArray());

                bool equals = true;

                try
                {
                    for (int i = 0; i < subItems.Count; i++)
                    {
                        if (list[i] != subItems[i].Trim())
                        {
                            equals = false;
                            break;
                        }
                    }
                }
                catch
                {
                    equals = false;
                }

                if (!equals && Program.Area == Program.MainArea)
                {
                    DirectoryPath = DirectoryPath;
                    Show();
                }
            }
        }
    }
}
