﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FarPrototype.View.Windows
{
    class BottomControlsWindow : IWindow
    {
        private List<string> commandsNames = new List<string>
        {
            "Help",
            "UserConf",
            "View",
            "Edit",
            "Copy",
            "Replace",
            "Find",
            "Delete",
            "ConfMn",
            "Quit",
            "Plugins",
            "Sreen"
        };

        public BottomControlsWindow(Position position, Size size) : base(position, size) { }

        public override void Show()
        {
            const int itemsCount = 12;
            int itemWidth = (Width - 3) / itemsCount;

            int emptyCellsCount = Width - itemWidth * itemsCount - 1;

            var startPositions = new List<int>();

            Console.ForegroundColor = ConsoleColor.White;
            Console.SetCursorPosition(PositionX, PositionY);
            for (int i = 0; i < itemsCount; i++)
            {
                Console.Write(i + 1);

                Console.BackgroundColor = ConsoleColor.DarkBlue;
                for (int j = 0; j < itemWidth - 2; j++)
                {
                    if(j == 0)
                        startPositions.Add(Console.CursorLeft);
                    Console.Write(' ');
                }

                if (i < emptyCellsCount - 1)
                    Console.Write(' ');

                Console.BackgroundColor = ConsoleColor.Black;
                Console.Write(' ');
            }

            PrintNames(startPositions, itemWidth);
        }

        private void PrintNames(List<int> startPositions, int itemWidth)
        {
            Console.ForegroundColor = ConsoleColor.Black;
            Console.BackgroundColor = ConsoleColor.DarkBlue;

            for (int i = 0; i < startPositions.Count; i++)
            {
                Console.SetCursorPosition(startPositions[i], PositionY);

                for (int j = 0; j < itemWidth - 2; j++)
                    if (j < commandsNames[i].Length)
                        Console.Write(commandsNames[i][j]);
                    else
                        break;
            }
        }
    }
}
