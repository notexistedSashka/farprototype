﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FarPrototype.View.Windows
{
    public abstract class IWindow
    {
        protected int width;
        protected int height;
        virtual public int Width { get => width; set => width = value; }
        virtual public int Height { get => height; set => height = value; }

        protected int positionX;
        protected int positionY;
        virtual public int PositionX { get => positionX; set => positionX = value; }
        virtual public int PositionY { get => positionY; set => positionY = value; }

        public IWindow(Position position, Size size)
        {
            PositionX = position.PositionX;
            PositionY = position.PositionY;
            Width = size.Width;
            Height = size.Height;
        }

        public abstract void Show();

        virtual public void SetPosition(int posX, int posY)
        {
            PositionX = posX;
            PositionY = posY;
        }

        virtual public void SetSize(int width, int height)
        {
            Width = width;
            Height = height;
        }
    }
}
