﻿using FarPrototype.Common;
using FarPrototype.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace FarPrototype.View.Windows
{
    class CmdWindow : IWindow
    {
        public List<string> Answers { get; set; }
        private string emptyLine = string.Empty;

        public CmdWindow(Position position, Size size) : base(position, size)
        {
            Answers = new List<string>();

            for (int i = PositionX; i < Width; i++)
                emptyLine += ' ';
        }

        private void PrintAnswers()
        {
            for (int i = 0; i < Height - 1; i++)
            {
                // Hide previous answer
                Console.SetCursorPosition(0, Height - 1 - i);
                Console.Write(emptyLine);

                // Print answer
                Console.SetCursorPosition(0, Height - 1 - i);
                if (Answers.Count > i)
                    Console.Write(Answers[Answers.Count - i - 1]);
                else
                    break;
            }

            Console.SetCursorPosition(CommonData.cursorCoords.Item1, CommonData.cursorCoords.Item2);
            Console.CursorVisible = true;
        }

        public override void Show()
        {
            PrintAnswers();
        }
    }
}
