﻿using FarPrototype.Common;
using FarPrototype.Connector;
using System;
using System.Collections.Generic;
using System.Text;

namespace FarPrototype.View
{
    public interface IHasHotkeys
    {
        Dictionary<string, IConnector> Commands { get; set; }
        Dictionary<string, IParameterizedConnector> ParameterizedCommands { get; set; }

        void Show();

        void BackSpaceClick(bool withCtrl);
        void EndClick();
        void HomeClick();
        void DeleteClick(bool withCtrl);
        void ArrowClick(Arrow arrow, bool withCtrl);

        void TypeSymbol(string s);
    }
}
