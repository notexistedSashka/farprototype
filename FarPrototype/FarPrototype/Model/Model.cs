﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Principal;

// Item - file or folder

namespace FarPrototype.Model
{
    public class Model
    {
        public void CopySourceTo(string sourcePath, string destPath)
        {
            string itemName = Path.GetFileName(sourcePath);

            if (Directory.Exists(sourcePath))
            {
                var sourceInfo = new DirectoryInfo(sourcePath);
                var destInfo = Directory.CreateDirectory(Path.Combine(destPath, itemName));

                foreach (var file in sourceInfo.GetFiles())
                    file.CopyTo(Path.Combine(destInfo.FullName, file.Name));

                foreach (var directory in sourceInfo.GetDirectories())
                    CopySourceTo(directory.FullName, destInfo.FullName);
            }
            else
            {
                string destFilePath = Path.Combine(destPath, itemName);
                var fileInfo = new FileInfo(sourcePath);
                fileInfo.CopyTo(destFilePath);
            }
        }

        public void ReplaceSourceTo(string sourcePath, string destPath)
        {
            string itemName = Path.GetFileName(sourcePath);

            destPath = destPath.Contains(Path.DirectorySeparatorChar)
                    ? Path.Combine(destPath, itemName)
                    : Path.Combine(sourcePath.Replace(itemName, string.Empty), destPath);

            if (Directory.Exists(sourcePath))
            {
                var sourceInfo = new DirectoryInfo(sourcePath);
                var destInfo = Directory.CreateDirectory(destPath);

                foreach (var file in sourceInfo.GetFiles())
                    file.MoveTo(Path.Combine(destInfo.FullName, file.Name));

                foreach (var directory in sourceInfo.GetDirectories())
                    ReplaceSourceTo(directory.FullName, destInfo.FullName);

                Directory.Delete(sourcePath, true);
            }
            else
            {
                var fileInfo = new FileInfo(sourcePath);
                fileInfo.MoveTo(destPath);
            }
        }

        public List<string> GetItemsFrom(string path)
        {
            var result = new List<string>();

            result.AddRange(Directory.GetDirectories(path));
            result.AddRange(Directory.GetFiles(path));

            for (int i = 0; i < result.Count; i++)
                result[i] = Path.GetFileName(result[i]);

            return result;
        }

        public string GetTextFromFile(string path)
        {
            return File.ReadAllText(path);
        }

        public void SetNewTextInFile(string path, string text)
        {
            File.WriteAllText(path, text);
        }

        public void DeleteItem(string path)
        {
            if (Directory.Exists(path))
                Directory.Delete(path, true);
            else
                File.Delete(path);
        }

        public void CreateDirectory(string path)
        {
            Directory.CreateDirectory(path);
        }

        public void CreateFile(string path)
        {
            if(!File.Exists(path))
                File.Create(path);
        }

        public DateTime GetDateTimeItem(string path)
        {
            return File.GetLastWriteTime(path);
        }

        public string GetFilesSizeStringFormat(string path)
        {
            long size = GetFilesSize(path);
            string res = $"{size} B";

            const int gb = 1073741824;
            const int mb = 1048576;
            const int kb = 1024;

            if (size >= gb)
                res = $"{Math.Round(size / Convert.ToDouble(gb), 2)} G";
            else if (size >= mb)
                res = $"{Math.Round(size / Convert.ToDouble(mb), 2)} M";
            else if (size >= kb)
                res = $"{Math.Round(size / Convert.ToDouble(kb), 2)} K";

            return res;
        }

        public long GetFilesSize(string path)
        {
            if (Directory.Exists(path))
            {
                long res = 0;

                foreach (var item in Directory.GetFiles(path))
                    res += GetFilesSize(item);

                return res;
            }
            else
            {
                var info = new FileInfo(path);
                return info.Length;
            }
        }

        public List<string> FindFiles(string path, string pattern, bool findInSubfolders)
        {
            SearchOption option = findInSubfolders ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly;
            var res = new List<string>();

            res.AddRange(Directory.GetFiles(path, pattern, option));

            return res;
        }

        // Adminrules
        static internal bool IsRunAsAdmin()
        {
            var id = WindowsIdentity.GetCurrent();
            var principal = new WindowsPrincipal(id);
            return principal.IsInRole(WindowsBuiltInRole.Administrator);
        }
    }
}
