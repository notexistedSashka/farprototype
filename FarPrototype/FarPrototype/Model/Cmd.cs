﻿using FarPrototype.Common;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;

namespace FarPrototype.Model
{
    class Cmd
    {
        static public StreamReader GetCommandResult(string path, string command)
        {
            // TODO: Сделать отдельную обработку exit и cd
            var procInfo = new ProcessStartInfo("cmd", $"/c {command} 2>&1");
            procInfo.WorkingDirectory = path;
            procInfo.RedirectStandardOutput = true;
            procInfo.UseShellExecute = false;
            procInfo.CreateNoWindow = true;
            var proc = new Process();
            proc.StartInfo = procInfo;

            proc.Start();

            return proc.StandardOutput;
        }
    }
}
