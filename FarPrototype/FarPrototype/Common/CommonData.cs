﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FarPrototype.Common
{
    public enum Resize
    {
        ToBigger,
        ToSmaller
    }

    public enum Pannel
    {
        Left,
        Right,
        Both
    }

    public enum Arrow
    {
        Left,
        Right,
        Up,
        Down,
        None
    }

    public struct CommonData
    {
        static public int minWidth = 80;
        static public int minHeight = 15;

        static public Tuple<int, int> cursorCoords = new Tuple<int, int>(0, 0);

        static public List<string> archiveExtensions = new List<string>{
            ".7z",
            ".ace",
            ".arj",
            ".bin",
            ".cab",
            ".cbr",
            ".deb",
            ".gz",
            ".gzip",
            ".jar",
            ".one",
            ".pak",
            ".pkg",
            ".rar",
            ".rpm",
            ".sh",
            ".sib",
            ".sis",
            ".sisx",
            ".sit",
            ".sitx",
            ".spl",
            ".tar",
            ".tar-gz",
            ".tgz",
            ".xar",
            ".zip",
            ".zipx"
            };
    }
}
